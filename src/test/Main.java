package test;

import java.util.logging.Logger;
import org.json.JSONObject;
import lib.LoggingConfigurator;


public class Main {
    
    static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
   	 LoggingConfigurator.configure();
   	 LOGGER.info(new JSONObject().put("hola", "mon").toString(4));
    }
}
