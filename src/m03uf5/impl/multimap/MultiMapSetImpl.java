package m03uf5.impl.multimap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class MultiMapSetImpl<K, V> extends MultiMapImpl<K, V>{
	Map<K, Collection<V>> map = new HashMap<K, Collection<V>>();
	
	@Override
	public Iterator<K> iterator() {
		return map.keySet().iterator();
	}

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public void put(K key, V value) {
		Collection<V> col = new HashSet<V>();
		if(map.containsKey(key)) {
			map.get(key).add(value);
		} else {
			col.add(value);
			map.put(key, col);
		}
	}

	@Override
	public Collection get(K key) {
		if(map.get(key) == null) {
			Collection<V> col = new HashSet<V>();
			map.put(key, col);
		}
		return map.get(key);
	}

	@Override
	public boolean remove(K key, V value) {
		return map.get(key).remove(value);
	}
	
}
