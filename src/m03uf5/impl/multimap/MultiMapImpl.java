package m03uf5.impl.multimap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import m03uf5.prob.multimap.MultiMap;

public abstract class MultiMapImpl<K, V> implements MultiMap<K, V> {
	@Override
	public abstract Iterator<K> iterator();

	@Override
	public abstract int size();

	@Override
	public abstract void put(K key, V value);

	@Override
	public abstract Collection get(K key);

	@Override
	public abstract boolean remove(K key, V value);

}
