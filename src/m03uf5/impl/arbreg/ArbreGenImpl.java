package m03uf5.impl.arbreg;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import m03uf5.prob.arbreg.ArbreGen;
import m03uf5.prob.arbreg.Parella;
import m03uf5.prob.arbreg.Persona;
import m03uf5.prob.arbreg.Sexe;

public class ArbreGenImpl implements ArbreGen{
    private String nom;
    private Sexe sexe;
    private Persona persona;
    List<Persona> arbre = new ArrayList<>();

    @Override
    public Iterator<Persona> iterator() {
        return arbre.iterator();
    }

    @Override
    public Parella<Persona, Persona> createPares(Persona pare, Persona mare) {
        Parella<Persona, Persona> parella = new ParellaImpl<Persona, Persona>(pare, mare);
        return parella;
    }

    @Override
    public Persona addPersona(String nom, Sexe sexe, Parella<Persona, Persona> pares) {
    	persona = new PersonaImpl(nom, pares, sexe);
        if(pares.getEsquerra() != null && pares.getDreta() != null) {
        	pares.getEsquerra().getFills().add(persona);
        	pares.getDreta().getFills().add(persona);
        }
        arbre.add(persona);
    	return persona;
    }

    @Override
    public Persona get(String nom) {
        for(int i = 0; i < arbre.size(); i++) if(arbre.get(i).getNom().equals(nom)) return arbre.get(i);
        return null;
    }

    @Override
    public Collection<Persona> getParelles(Persona p) {
    	Collection<Persona> tempCol = new ArrayList<>();
    	for(Persona hijo : p.getFills()) {
    		if(hijo.getPares().getDreta() == p && tempCol.contains(hijo.getPares().getEsquerra()) == false) {
    			tempCol.add(hijo.getPares().getEsquerra());
    		} else if(hijo.getPares().getEsquerra() == p && tempCol.contains(hijo.getPares().getDreta()) == false) {
    			tempCol.add(hijo.getPares().getDreta());
    		}
    	}
        return tempCol;
    }

    @Override
    public Collection<Persona> getFillsEnComu(Persona pare, Persona mare) {
    	Collection<Persona> tempCol = new ArrayList<>();
    	for(Persona hM : mare.getFills()) {
    		for(Persona hP : pare.getFills()) {
    			if(hM.equals(hP)) tempCol.add((Persona) hM);
    		}
    	}
        return tempCol;
    }

}