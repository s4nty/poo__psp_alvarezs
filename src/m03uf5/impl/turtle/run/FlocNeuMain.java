package m03uf5.impl.turtle.run;

import lib.Problems;
import lib.Reflections;
import m03uf5.prob.turtle.Canvas;
import m03uf5.prob.turtle.Turtle;

public class FlocNeuMain {
    public static void main(String[] args) {
        String packageName = Problems.getImplPackage(Canvas.class);
        Canvas canvas = Reflections.newInstanceOfType(Canvas.class, packageName + ".CanvasImpl");
        Turtle turtle = Reflections.newInstanceOfType(Turtle.class, packageName + ".TurtleImpl");
        FlocNeuFigure ffii = new FlocNeuFigure();
        canvas.setScale(100);
        turtle.setCanvas(canvas);
        turtle.up();
        turtle.goForward(50);
        turtle.turnLeft(90);
        turtle.goForward(50);
        turtle.down();
        ffii.draw(turtle);
    }
}