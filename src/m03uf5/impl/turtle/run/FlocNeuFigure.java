package m03uf5.impl.turtle.run;

import m03uf5.prob.turtle.Figure;
import m03uf5.prob.turtle.Turtle;
import m03uf5.impl.turtle.run.FlorFigure;;

public class FlocNeuFigure implements Figure{
    @Override
    public void draw(Turtle t) {

        FlorFigure fi = new FlorFigure();

        for(int i = 0; i <= 360; i+=45) {
        	fi.draw(t);
        	t.turnLeft(i);
        }


    }

}