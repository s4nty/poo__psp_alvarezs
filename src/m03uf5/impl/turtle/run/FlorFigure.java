package m03uf5.impl.turtle.run;

import m03uf5.prob.turtle.Figure;
import m03uf5.prob.turtle.Turtle;

public class FlorFigure implements Figure{

    @Override
    public void draw(Turtle t) {
        int branca = 25;
        int petal = 5;

        t.goForward(branca);

        t.turnLeft(45);
        t.goForward(petal);
        t.turnRight(90);
        t.goForward(petal);
        t.turnRight(90);
        t.goForward(petal);
        t.turnRight(90);
        t.goForward(petal);
        t.turnLeft(45);

        t.goForward(branca);
        t.turnLeft(180);

    }

}