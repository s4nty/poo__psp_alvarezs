package m03uf5.impl.turtle;

import lib.StdDraw;
import m03uf5.prob.turtle.Canvas;
import m03uf5.prob.turtle.Turtle;

public class TurtleImpl implements Turtle{
	private double x = 0;
	private double y = 0;
	private double angle = 0;
	private boolean write = false;
	private Canvas canvas;
	@Override
	public void up() {
		// TODO Auto-generated method stub
		write = false;
	}

	@Override
	public void down() {
		// TODO Auto-generated method stub
		write = true;
	}

	@Override
	public double x() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double y() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double angle() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void turnLeft(double delta) {
		angle += delta;
	}

	@Override
	public void turnRight(double delta) {
		// TODO Auto-generated method stub
		angle -= delta;
	}

	@Override
	public void goForward(double step) {
		// TODO Auto-generated method stub
		double oldx = x;
        double oldy = y;
        x += step * Math.cos(Math.toRadians(angle));
        y += step * Math.sin(Math.toRadians(angle));
        if(write) StdDraw.line(oldx, oldy, x, y);
	}

	@Override
	public void goBackward(double step) {
		// TODO Auto-generated method stub
		goForward(-step);
		
	}

	@Override
	public void setCanvas(Canvas canvas) {
		// TODO Auto-generated method stub
		this.canvas = canvas;
	}

	@Override
	public Canvas getCanvas() {
		// TODO Auto-generated method stub
		return null;
	}

}
