package m03uf5.impl.turtle;

import lib.StdDraw;
import m03uf5.prob.turtle.Canvas;
import m03uf5.prob.turtle.Color;

public class CanvasImpl implements Canvas {

	public void setScale(double scale) {
		StdDraw.setScale(0, scale);
	}

	public double getWidth() {
		// TODO Auto-generated method stub
		return 0;
	}

	public double getHeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setColor(Color color) {
		// black, red, yellow, blue, green
		switch(color) {
			case BLACK:
				StdDraw.setPenColor(StdDraw.BLACK);
				return;
			case RED: 
				StdDraw.setPenColor(StdDraw.RED);
				return;
			case YELLOW:
				StdDraw.setPenColor(StdDraw.YELLOW);
				return;
			case BLUE:
				StdDraw.setPenColor(StdDraw.BLUE);
				return;
			case GREEN:
				StdDraw.setPenColor(StdDraw.GREEN);
				return;
		}
	}

	public void setPenSize(double size) {
		size *= 0.002;
		StdDraw.setPenRadius(size);
		
	}

	public void drawPoint(double x, double y) {
		StdDraw.point(x, y);
		
	}

	public void drawLine(double x0, double y0, double x1, double y1) {
		StdDraw.line(x0, y0, x1, y1);
		
	}

	public void save(String filename) {
		StdDraw.save(filename);
		
	}

}
