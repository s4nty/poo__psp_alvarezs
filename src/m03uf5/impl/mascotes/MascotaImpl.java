package m03uf5.impl.mascotes;

import m03uf5.prob.mascotes.Especie;
import m03uf5.prob.mascotes.Llar;
import m03uf5.prob.mascotes.Mascota;
import m03uf5.prob.mascotes.Xip;

public class MascotaImpl implements Mascota {
	private String nom;
	private int any;
	private Llar llar;
	private Especie especie;
	private Xip xip;
	public MascotaImpl(String nom, int any, Llar llar, Especie especie, Xip xip) {
		this.nom = nom;
		this.any = any;
		this.llar = llar;
		this.especie = especie;
		this.xip = xip;
	}
	public String toString() {
		return nom+" ("+any+") es un "+ especie +" amb chip "+ xip.getId() +" a "+ llar.getNom();
	}
	public MascotaImpl(Xip xip) {
		this.xip = xip;
	}
	@Override
	public int compareTo(Mascota m) {
		if(m.getAny() < any) {
			return 1;
		} else if(m.getAny() > any) {
			return -1;
		} else {
			return 0;
		}
	}

	@Override
	public Xip getXip() {
		return xip;
	}

	@Override
	public String getNom() {
		return nom;
	}

	@Override
	public int getAny() {
		return any;
	}

	@Override
	public Especie getEspecie() {
		return especie;
	}

}
