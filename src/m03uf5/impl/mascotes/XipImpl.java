package m03uf5.impl.mascotes;

import m03uf5.prob.mascotes.Llar;
import m03uf5.prob.mascotes.Xip;

public class XipImpl implements Xip {
	private Long id;
	private Llar llar;
	public XipImpl(Long id, Llar llar) {
		this.id = id;
		this.llar = llar;
	}
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public Llar getLlar() {
		return llar;
	}

}
