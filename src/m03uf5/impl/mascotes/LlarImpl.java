package m03uf5.impl.mascotes;

import m03uf5.prob.mascotes.Llar;

public class LlarImpl implements Llar {
	private String nom;
	public LlarImpl(String nom) {
		this.nom = nom;
	}
	@Override
	public String getNom() {
		return nom;
	}
	
}
