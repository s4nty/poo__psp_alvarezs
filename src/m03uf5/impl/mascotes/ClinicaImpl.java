package m03uf5.impl.mascotes;

import java.util.Map;
import java.util.Map.Entry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import m03uf5.prob.mascotes.Clinica;
import m03uf5.prob.mascotes.Especie;
import m03uf5.prob.mascotes.Llar;
import m03uf5.prob.mascotes.Mascota;
import m03uf5.prob.mascotes.Veterinari;

public class ClinicaImpl implements Clinica {
	Map<Long, Mascota> mascotas = new HashMap<>();
	List<Mascota> masc = new ArrayList<>();
	//private int xip = 0;
	private Long xip = 1l;
	@SuppressWarnings("unused")
	private XipImpl xipP;
	@Override
	public Llar creaLlar(String nom) {
		Llar creaLlar = new LlarImpl(nom);
		return creaLlar;
	}

	@Override
	public Veterinari creaVeterinari(String nom) {
		VeterinariImpl vet = new VeterinariImpl(nom);
		return vet;
	}

	@Override
	public Mascota registraGos(String nom, int any, Llar llar) {
		Especie especie = Especie.GOS;
		xipP = new XipImpl(xip, llar);
		Mascota animal = new MascotaImpl(nom, any, llar, especie, xipP);
		mascotas.put(xip, animal);
		xip++;
		return animal;
	}

	@Override
	public Mascota registraGat(String nom, int any, Llar llar) {
		Especie especie = Especie.GAT;
		xipP = new XipImpl(xip, llar);
		Mascota animal = new MascotaImpl(nom, any, llar, especie, xipP);
		mascotas.put(xip, animal);
		xip++;
		return animal;
	}

	@Override
	public Mascota trobaMascota(Long xipId) {
		// TODO Auto-generated method stub
		return mascotas.get(xipId);
	}

	@Override
	public List<Mascota> llistaMascotes(Llar llar) {
		for (Entry<Long, Mascota> entry: mascotas.entrySet()) {
			//System.out.println(entry.getKey() + " => " + entry.getValue());
			//System.out.println(entry.getValue().getXip().getLlar());
			if(llar.getNom().equals(entry.getValue().getXip().getLlar().getNom())){
                masc.add(entry.getValue());
              //  System.out.println(entry.getValue());
            }
		}
		return masc;
	}
}
