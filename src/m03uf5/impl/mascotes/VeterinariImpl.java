package m03uf5.impl.mascotes;

import java.util.ArrayList;
import java.util.List;

import m03uf5.prob.mascotes.Mascota;
import m03uf5.prob.mascotes.Veterinari;

public class VeterinariImpl implements Veterinari {
	private String nom;
	private int pendents;
	private List<Mascota> mascV = new ArrayList<>();
	public VeterinariImpl(String nom) {
		this.nom = nom;
	}
	@Override
	public String getNom() {
		return nom;
	}

	@Override
	public int pendents() {
		return pendents;
	}

	@Override
	public void visita(Mascota mascota) {
		mascV.add(mascota);
		pendents++;
	}

	@Override
	public Mascota atendre() {
		if(pendents > 0) {
			Mascota temp = mascV.get(0);
			mascV.remove(0);
			pendents--;
			return temp;
		} 
		return null;
	}
}
