
package m03uf5.impl.exam1b;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import m03uf5.prob.exam1b.Comanda;
import m03uf5.prob.exam1b.Establiment;
import m03uf5.prob.exam1b.Proveidor;

public class EstablimentImpl<T> implements Establiment<T> {
	Map<T, Integer> map = new HashMap<T, Integer>();
	Map<T, Proveidor<T>> provs = new HashMap<T, Proveidor<T>>();
	@Override
	public void setProveidor(Proveidor<T> p) {
		map.put(p.getProducte(), 0);
		provs.put(p.getProducte(), p);
	}

	@Override
	public Proveidor<T> getProveidor(T t) {
		return provs.get(t);
	}

	@Override
	public int getEstoc(T t) {
		if(!provs.containsKey(t)) {
			map.put(t, 0);
		}
		return map.get(t);
	}

	@Override
	public List<Comanda<T>> processar(List<Comanda<T>> comandes) {
		List<Comanda<T>> noPro = new ArrayList<Comanda<T>>();
		for(Comanda<T> c: comandes) {
			T producteTemp = c.getProducte();
			int quantitatTemp = c.getQuantitat();
			Proveidor<T> provTemp = provs.get(producteTemp);
			if(provTemp != null) {
				int sumaP = provTemp.subministrar(quantitatTemp);
				if(sumaP > 0) {
					map.replace(producteTemp, (map.get(producteTemp) + sumaP));	
				} else {
					noPro.add(c);
				}
			} else {
				noPro.add(c);
			}
			
		}
		return noPro;
	}
	

}
