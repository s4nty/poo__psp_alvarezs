package m03uf5.impl.exam1b;

import m03uf5.prob.exam1b.Proveidor;

public class ProveidorImpl<T> implements Proveidor<T> {
	T producte;
	int estoc;
	public ProveidorImpl(T producte, int estoc) {
		this.producte = producte;
		this.estoc = estoc;
	}
	
	@Override
	public T getProducte() {
		return producte;
	}

	@Override
	public int getEstoc() {
		return estoc;
	}

	@Override
	public void produir(int quantitat) {
		estoc += quantitat;
		
	}

	@Override
	public int subministrar(int quantitat) {
		int control = estoc - quantitat;
		if(control > 0) {
			estoc -= quantitat;
			return quantitat;
		} else {
			return 0;			
		}

	}


}
