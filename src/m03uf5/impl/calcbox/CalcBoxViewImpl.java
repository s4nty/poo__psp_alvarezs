package m03uf5.impl.calcbox;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import m03uf5.prob.calcbox.CalcBoxContract.CalcBoxPresenter;
import m03uf5.prob.calcbox.CalcBoxContract.CalcBoxView;

public class CalcBoxViewImpl implements CalcBoxView {
	private Label label;
	private Button btn;
	private TextField txt;
	private CalcBoxPresenter cbp;
	
	public CalcBoxViewImpl(Stage p) {
		p.setTitle("CalcBoxApp");
		txt = new TextField();
		txt.setText("");
		label = new Label();
		label.setText("");
		btn = new Button();
		btn.setText("CALCULA");
		btn.setOnAction(event -> cbp.calcula(txt.getText()));
		VBox root = new VBox();
		root.setAlignment(Pos.CENTER);
		root.getChildren().addAll(txt, btn, label);
		p.setScene(new Scene(root, 500, 500));
		p.show();
		
		
	}
	@Override
	public void setPresenter(CalcBoxPresenter p) {
		cbp = p;
		
	}
	@Override
	public void mostra(String resultat) {
		label.setText(resultat);
		
	}
}
