package m03uf5.impl.calcbox;

public class ExprToken {

	private boolean isNumber, isOp, isParenthesis, isOpen;
	private double number;
	private char op;
	
	/**
	 * creates a number token
	 * @param number
	 */
	public ExprToken(double number) {
		isNumber = true;
		this.number = number;
	}
	
	/**
	 * creates an operation token
	 * @param op
	 */
	public ExprToken(char op) {
		isOp = true;
		this.op = op;
	}
	
	/**
	 * creates a parenthesis token, open or closed
	 * @param isOpen
	 */
	public ExprToken(boolean isOpen) {
		isParenthesis = true;
		this.isOpen = isOpen;
	}
	
	/**
	 * true if is an open parenthesis
	 * @return
	 */
	public boolean isOpen() {
		return isParenthesis && isOpen;
	}
	
	/**
	 * true if is a closed parenthesis
	 * @return
	 */
	public boolean isClose() {
		return isParenthesis && !isOpen;
	}
	
	/**
	 * true if is an operation
	 * @return
	 */
	public boolean isOp() {
		return isOp;
	}
	
	/**
	 * gets the operation
	 * @return
	 */
	public char getOp() {
		return op;
	}
	
	/**
	 * true if is a number token
	 * @return
	 */
	public boolean isNumber() {
		return isNumber;
	}
	
	/**
	 * gets the number
	 * @return
	 */
	public double getNumber() {
		return number;
	}
	
	@Override
	public String toString() {
		if (isOpen()) {
			return Character.toString('(');
		}
		else if (isClose()) {
			return Character.toString(')');
		}
		else if (isOp()) {
			return Character.toString(op);
		}
		else {
			return Double.toString(number);
		}
	}
}
