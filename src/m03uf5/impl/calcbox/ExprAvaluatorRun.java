package m03uf5.impl.calcbox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ExprAvaluatorRun {

	public static void main(String[] args) throws IOException {
		
		// exemple d'us de l'avaluador d'expressions
		
		ExprAvaluator ea = new ExprAvaluator();
		
		try (BufferedReader r = new BufferedReader(new InputStreamReader(System.in))) {
			
			while (true) {
				System.out.print("> ");
				String line = r.readLine();
				System.out.println(ea.avalua(line));
			}
		}
	}
}
