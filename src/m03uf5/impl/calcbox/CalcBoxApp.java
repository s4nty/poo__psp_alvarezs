package m03uf5.impl.calcbox;

import javafx.application.*;
import javafx.stage.Stage;

public class CalcBoxApp extends Application {
	private CalcBoxViewImpl v;
	private CalcBoxPresenterImpl p;
	public static void main(String[] args) {
		launch(args);
	}
	
	public void start(Stage arg0) throws Exception {
		p = new CalcBoxPresenterImpl();
		v = new CalcBoxViewImpl(arg0);
		
		
		p.setView(v);
		v.setPresenter(p);
		
	}

}
