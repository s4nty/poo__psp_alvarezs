package m03uf5.impl.calcbox;

import java.io.IOException;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/*
 * Avaluador d'expressions, permet les quatre operacions bàsiques, parèntisi i números amb decimals
 */
public class ExprAvaluator {
		
	public double avalua(String expr) throws IOException {
		
		List<ExprToken> iTokens = readInfixTokens(expr);
		List<ExprToken> pTokens = convertToPostfix(iTokens);
		return evaluate(pTokens);
	}

	private List<ExprToken> readInfixTokens(String expr) throws IOException {
		
		List<ExprToken> tokens = new ArrayList<ExprToken>();

		StreamTokenizer st = new StreamTokenizer(new StringReader(expr));
		st.ordinaryChar('/');
		
		int currentToken = st.nextToken();
		while (currentToken != StreamTokenizer.TT_EOF) {

			if (st.ttype == StreamTokenizer.TT_NUMBER) {
				
				if (st.nval < 0) {
					tokens.add(new ExprToken('-'));
					tokens.add(new ExprToken(-st.nval));
				}
				else {
					tokens.add(new ExprToken(st.nval));	
				}
			
			} else if (st.ttype == '(') {
				tokens.add(new ExprToken(true));
			} else if (st.ttype == ')') {
				tokens.add(new ExprToken(false));
			} else {
				tokens.add(new ExprToken((char) currentToken));			
			}

			currentToken = st.nextToken();
		}

		return tokens;
	}

	private List<ExprToken> convertToPostfix(List<ExprToken> tokens) {
		
		Deque<ExprToken> stack = new LinkedList<ExprToken>();
		List<ExprToken> output = new ArrayList<>();

		for (ExprToken token: tokens) {
			// operator
			if (token.isOp()) {
				while (!stack.isEmpty() && isHigerPrec(token.getOp(), stack.peek().getOp())) {
					output.add(stack.pop());
				}
				stack.push(token);

				// left parenthesis
			} else if (token.isOpen()) {
				stack.push(token);

				// right parenthesis
			} else if (token.isClose()) {
				while (!stack.peek().isOpen()) {
					output.add(stack.pop());
				}
				stack.pop();

				// digit
			} else {
				output.add(token);
			}
			
			//System.out.println(token + ": " + output + " <====> " + stack);
		}
		
		while (!stack.isEmpty()) {
			output.add(stack.pop());
		}

		return output;
	}

	private double evaluate(List<ExprToken> tokens) {

		Deque<Double> stack = new LinkedList<>();		
		for (ExprToken token: tokens) {
			
			if (token.isOp()) {
				char op = token.getOp();
				switch (op) {
				case '+':
					stack.push(stack.pop() + stack.pop());
					break;
				case '-':
					stack.push(-stack.pop() + stack.pop());
					break;
				case '*':
					stack.push(stack.pop() * stack.pop());
					break;
				case '/':
					stack.push(1 / (stack.pop() / stack.pop()));
					break;
				}
			}
			else {
				stack.push(token.getNumber());
			}			
		}
		
		return stack.pop();
	}

	// OPERATORS
	
	private enum Operator {
		ADD(1), SUBTRACT(2), MULTIPLY(3), DIVIDE(4);

		final int precedence;

		Operator(int p) {
			precedence = p;
		}
	}

	private static Map<Character, Operator> ops = new HashMap<Character, Operator>() {
		private static final long serialVersionUID = 1L;

		{
			put('+', Operator.ADD);
			put('-', Operator.SUBTRACT);
			put('*', Operator.MULTIPLY);
			put('/', Operator.DIVIDE);
		}
	};	
	
	private static boolean isHigerPrec(char op, char sub) {
		return (ops.containsKey(sub) && ops.get(sub).precedence >= ops.get(op).precedence);
	}	
}
