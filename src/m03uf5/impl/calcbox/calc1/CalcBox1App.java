package m03uf5.impl.calcbox.calc1;

import java.io.IOException;
import java.util.NoSuchElementException;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import m03uf5.prob.calcbox.ExprAvaluator;

public class CalcBox1App extends Application {
	private Label label;
	private Button btn;
	private TextField txt;
	private ExprAvaluator ea;
	
	public static void main(String[] args) {
		launch(args);
	}

	public void start(Stage p) {
		p.setTitle("CalcBoxApp");
		txt = new TextField();
		txt.setText("");
		label = new Label();
		label.setText("");
		btn = new Button();
		btn.setText("CALCULA");
		btn.setOnAction(event -> calcula(txt.getText()));
		VBox root = new VBox();
		root.setAlignment(Pos.CENTER);
		root.getChildren().addAll(txt, btn, label);
		p.setScene(new Scene(root, 500, 500));
		p.show();
	}
	
	public void calcula(String expressio) {
		ea = new ExprAvaluator();
		try {
			label.setText(String.valueOf(ea.avalua(expressio)));
		} catch (IOException e) {
			System.out.println("F");
		} catch (NoSuchElementException ns) {
			label.setText("Has introducido un car�cter inv�lido");
		}
		
	}
	
}
