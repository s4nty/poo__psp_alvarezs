package m03uf5.impl.calcbox;

import java.io.IOException;
import java.util.NoSuchElementException;

import m03uf5.prob.calcbox.CalcBoxContract.CalcBoxPresenter;
import m03uf5.prob.calcbox.CalcBoxContract.CalcBoxView;
import m03uf5.prob.calcbox.ExprAvaluator;

public class CalcBoxPresenterImpl implements CalcBoxPresenter {
	private CalcBoxView cbv;
	private ExprAvaluator ea;
	
	public CalcBoxPresenterImpl() {
		ea = new ExprAvaluator();
	}
	
	@Override
	public void setView(CalcBoxView v) {
		cbv = v;
		
	}

	@Override
	public void calcula(String expressio) {
		try {
			cbv.mostra(String.valueOf(ea.avalua(expressio)));
		} catch (IOException e) {
			System.out.println("F");
		} catch (NoSuchElementException ns) {
			cbv.mostra("Has introducido un car�cter inv�lido");
		}
		
	}
}
