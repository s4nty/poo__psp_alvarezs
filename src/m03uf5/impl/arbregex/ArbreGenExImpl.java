package m03uf5.impl.arbregex;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import lib.db.DuplicateException;
import lib.db.NotFoundException;
import m03uf5.impl.arbreg.ParellaImpl;
import m03uf5.impl.arbreg.PersonaImpl;
import m03uf5.prob.arbreg.Parella;
import m03uf5.prob.arbreg.Persona;
import m03uf5.prob.arbreg.Sexe;
import m03uf5.prob.arbregex.ArbreGenEx;

public class ArbreGenExImpl implements ArbreGenEx {
	private String nom;
	private Sexe sexe;
	private Persona persona;
	List<Persona> arbre = new ArrayList<>();

	@Override
	public Iterator<Persona> iterator() {
		return arbre.iterator();
	}

	@Override
	public Parella<Persona, Persona> createPares(Persona pare, Persona mare) {
		Parella<Persona, Persona> parella = new ParellaImpl<Persona, Persona>(pare, mare);
		return parella;
	}

	@Override
	public Persona addPersona(String nom, Sexe sexe, Parella<Persona, Persona> pares) throws DuplicateException {
		persona = new PersonaImpl(nom, pares, sexe);
		if (pares != null && nom != null && sexe != null) {
			if (pares.getEsquerra() != null && pares.getDreta() != null) {
				pares.getEsquerra().getFills().add(persona);
				pares.getDreta().getFills().add(persona);
			}
			for(Persona p : arbre) if(p.getNom().equals(nom)) throw new DuplicateException(nom);
			arbre.add(persona);
			return persona;
		} else {
			throw new IllegalArgumentException(nom + ", " + sexe + ", " + pares);
		}

	}

	@Override
	public Persona get(String nom) throws NotFoundException {
		if(nom != null) {
			for (int i = 0; i < arbre.size(); i++)
				if (arbre.get(i).getNom().equals(nom))
					return arbre.get(i);
			throw new NotFoundException(nom);
		} else {
			throw new IllegalArgumentException();
		}
		

		

	}

	@Override
	public Collection<Persona> getParelles(Persona p) {
		Collection<Persona> tempCol = new ArrayList<>();
		if (p != null) {
			for (Persona hijo : p.getFills()) {
				if (hijo.getPares().getDreta() == p && tempCol.contains(hijo.getPares().getEsquerra()) == false) {
					tempCol.add(hijo.getPares().getEsquerra());
				} else if (hijo.getPares().getEsquerra() == p
						&& tempCol.contains(hijo.getPares().getDreta()) == false) {
					tempCol.add(hijo.getPares().getDreta());
				}
			}
			return tempCol;
		} else {
			throw new IllegalArgumentException();
		}
	}

	@Override
	public Collection<Persona> getFillsEnComu(Persona pare, Persona mare) throws IllegalArgumentException {
		Collection<Persona> tempCol = new ArrayList<>();
		if (pare != null && mare != null) {
			for (Persona hM : mare.getFills()) {
				for (Persona hP : pare.getFills()) {
					if (hM.equals(hP))
						tempCol.add((Persona) hM);
				}
			}
			return tempCol;
		} else {
			throw new IllegalArgumentException(pare + ", " + mare);
		}
	}
}
