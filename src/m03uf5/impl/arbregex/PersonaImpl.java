package m03uf5.impl.arbregex;

import java.util.ArrayList;
import java.util.Collection;

import m03uf5.prob.arbreg.Parella;
import m03uf5.prob.arbreg.Persona;
import m03uf5.prob.arbreg.Sexe;

public class PersonaImpl implements Persona {
	private Sexe sexe;
	private String nom;
	private Parella<Persona, Persona> parella;
	private Collection<Persona> fill = new ArrayList<>();
	
	public PersonaImpl(String nom, Parella parella, Sexe sexe) {
		this.nom = nom;
		this.parella = parella;
		this.sexe = sexe;
	}
	@Override
	public String getNom() {
		return nom;
	}

	@Override
	public Sexe getSexe() {
		return sexe;
	}

	@Override
	public Parella<Persona, Persona> getPares() {
		return parella;
	}

	@Override
	public Collection<Persona> getFills() {
		return fill;
	}
	
	public String toString() {
		return nom;
	}
	
}
