package m03uf5.impl.arbregex;

import m03uf5.prob.arbreg.Parella;

public class ParellaImpl<E, D> implements Parella<E, D> {
	public E e;
	public D d;
	
	public ParellaImpl(E e, D d) {
		this.e = e;
		this.d = d;	
	}
	@Override
	public E getEsquerra() {
		return e;
	}

	@Override
	public D getDreta() {
		return d;
	}
	
	public String toString() {
		if(e == null && d == null) return "[E = null, D = null]";
		return "[E = " + e + ", D = " + d + "]";
	}

}
