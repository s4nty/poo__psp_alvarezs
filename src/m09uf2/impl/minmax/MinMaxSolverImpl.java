package m09uf2.impl.minmax;

import m09uf2.prob.minmax.MinMaxSolver;

public class MinMaxSolverImpl implements MinMaxSolver, Runnable {
	private MinMaxCalc calc;
	private Thread threads[];
	public MinMaxSolverImpl(MinMaxCalc calc) {
		this.calc = calc;
	}
	
	@Override
	public double[] minMax(double[] nums) {
		synchronized(nums) {
			int x = nums.length / Runtime.getRuntime().availableProcessors();
			for(int i = 0; i < Runtime.getRuntime().availableProcessors(); i++) {
				threads[i] = new Thread();
				threads[i].start();
			}
			return new double[]{calc.min(nums, 0, nums.length), calc.max(nums, 0, nums.length)};
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}

}
