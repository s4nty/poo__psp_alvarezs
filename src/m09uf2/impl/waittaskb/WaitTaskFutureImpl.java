package m09uf2.impl.waittaskb;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import m09uf2.prob.waittaskb.DoTask;
import m09uf2.prob.waittaskb.WaitTaskFuture;

public class WaitTaskFutureImpl<T> implements WaitTaskFuture<T>, Runnable {
	static final Logger LOGGER = Logger.getLogger(WaitTaskJoinImpl.class.getName());
	private long millis;
	private Thread thread;
	private DoTask<T> task;
	private CompletableFuture<T> cf = new CompletableFuture<>();
	public WaitTaskFutureImpl() {
		this.thread = new Thread(this, "WaitTaskFutureImpl");
	}
	@Override
	public void submit(long millis, DoTask<T> task) {
		this.millis = millis;
		this.task = task;
		this.thread.start();
	}

	@Override
	public Future<T> future() {
		return this.cf;
	}

	@Override
	public void cancel(T t) {
		cf.complete(t);
		this.thread.interrupt();
	}

	@Override
	public void run() {
		try {
			Thread.sleep(this.millis);
			cf.complete(this.task.execute());
		} catch (InterruptedException e) {
			LOGGER.info("ˇInterrumpido!");
		}
		
	}



}
