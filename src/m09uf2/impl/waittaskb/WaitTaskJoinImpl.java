package m09uf2.impl.waittaskb;

import java.util.logging.Logger;

import m09uf2.prob.waittaskb.DoTask;
import m09uf2.prob.waittaskb.WaitTaskJoin;

public class WaitTaskJoinImpl<T> implements WaitTaskJoin<T>, Runnable {
	static final Logger LOGGER = Logger.getLogger(WaitTaskJoinImpl.class.getName());
	private long millis;
	private Thread thread;
	private DoTask<T> task;
	private T resultado;
	public WaitTaskJoinImpl() {
		this.thread = new Thread(this, "WaitTaskJoinImpl");
	}
	@Override
	public void submit(long millis, DoTask<T> task) {
		this.millis = millis;
		this.task = task;
		this.thread.start();
	}

	@Override
	public T result() {
		try {
			this.thread.join();
			return this.resultado;
		} catch (InterruptedException e) {
			LOGGER.info("ˇInterrumpido!");
		}
		return this.resultado;
	}

	@Override
	public void cancel(T t) {
		this.resultado = t;
		this.thread.interrupt();
	}

	@Override
	public void run() {
		try {
			LOGGER.info("Millis: " + this.millis);
			Thread.sleep(this.millis);
			this.resultado = this.task.execute();
			LOGGER.info("Result: " + this.resultado);
		} catch (InterruptedException e) {
			LOGGER.info("ˇInterrumpido!");
		}
	}


}
