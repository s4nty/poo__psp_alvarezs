package m09uf2.impl.timer2;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import m09uf2.prob.timer2.Timer2;

public class Timer2Impl implements Timer2, Runnable {
	private int id = 0;
	private Map<Integer, Pair> mapa = new HashMap<Integer, Pair>();
	private boolean running = true;
	private Thread thread;
	public Timer2Impl() {
		this.thread = new Thread(this, "fill");
		this.thread.start();
	}
	@Override
	public int set(long millis, Consumer<Integer> handler) {
		mapa.put(this.id++, new Pair(millis, handler, false));
		return this.id;
	}

	@Override
	public void shutdown() {
		this.running = false;
	}

	@Override
	public void run() {
		int x = 0;
		long milisRestar = 0;
		long millis = Long.MAX_VALUE;
		while(this.running) {
			if(mapa.size() > 0) {
				try {
					Thread.sleep(100);
					for(Map.Entry<Integer, Pair> e : mapa.entrySet()) {
						long tempMillis = e.getValue().getMillis();
						if(tempMillis < millis && !e.getValue().getDone()) {
							millis = tempMillis;
							x = e.getKey();
						}
					}
					Pair temp = mapa.get(x);
					thread.join(millis - milisRestar);
					milisRestar = millis;
					temp.getHandler().accept(x);
					temp.setDone(true);
					millis = Long.MAX_VALUE;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			
 			
		}
	}

}
