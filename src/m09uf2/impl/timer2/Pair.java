package m09uf2.impl.timer2;

import java.util.function.Consumer;

public class Pair {
	private long millis;
	private Consumer<Integer> handler;
	private boolean done;
	
	public Pair(long millis, Consumer<Integer> handler, boolean done) {
		this.millis = millis;
		this.handler = handler;
		this.done = done;
	}
	
	public long getMillis() {
		return this.millis;
	}
	
	public Consumer<Integer> getHandler() {
		return this.handler;
	}
	
	public boolean getDone() {
		return this.done;
	}
	
	public void setDone(boolean done) {
		this.done = done;
	}
}
