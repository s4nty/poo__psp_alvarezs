package m09uf2.impl.timer1;

import m09uf2.prob.timer1.Timer1;

public class Timer1Impl implements Timer1, Runnable {
	private long millis;
	private Thread thread;
	public Timer1Impl(long millis) {
		this.millis = millis;
		this.thread = new Thread(this, "fill");
	}
	@Override
	public long millis() {
		return this.millis;
	}

	@Override
	public void go() {
		this.thread.start();
	}

	@Override
	public void cancel() {
		this.thread.interrupt();
	}

	@Override
	public void hold() {
		try {
			this.thread.join(this.millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean done() {
		return !this.thread.isAlive();
	}

	@Override
	public Thread getThread() {
		return this.thread;
	}
	@Override
	public void run() {
		try {
			Thread.sleep(this.millis);
		} catch (InterruptedException e) {
			System.out.println("Bye bye");
		}
		
	}

}
