package m09uf2.impl.votacio;

import m09uf2.prob.votacio.LocalElectoral;

public class LocalElectoralImpl implements LocalElectoral {
	
	private VotCalc vc;
	private int mesas;
	private int candidatos;
	private int presentes;
	private int[][] votos;
	private int[] resultadosFinales;
	
	public LocalElectoralImpl(VotCalc vc, int mesas, int candidatos) {
		super();
		this.vc = vc;
		this.mesas = mesas;
		this.candidatos = candidatos;
		this.votos = new int[mesas][candidatos];
		this.resultadosFinales = new int[candidatos];
	}

	@Override
	public int findMesa(long idVotant) {
        return Math.toIntExact(idVotant % this.mesas);
	}

	@Override
	public boolean votar(long idVotant, int mesa, int candidat) {
		synchronized(this.votos[mesa]) {
			if(this.findMesa(idVotant) == mesa && candidat >= 0 && candidat <= this.candidatos) {
				this.votos[mesa][candidat] = vc.increment(this.votos[mesa][candidat]);
				return true;
			}
			return false;
		}
		
	}

	@Override
	public synchronized void entrada() {
		this.presentes++;
		
	}

	@Override
	public synchronized void sortida() {
		this.presentes--;
		
	}

	@Override
	public int presents() {
		return this.presentes;
	}

	@Override
	public int[] resultats() {
		for(int i = 0; i < this.candidatos; i++) {
			for(int j = 0; j < this.mesas; j++) {
				this.resultadosFinales[i] += this.votos[j][i];
			}
		}
		return this.resultadosFinales;
	}

}
