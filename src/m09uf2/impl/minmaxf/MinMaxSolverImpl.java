package m09uf2.impl.minmaxf;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import m09uf2.prob.minmaxf.MinMaxSolver;



public class MinMaxSolverImpl implements MinMaxSolver {

static final Logger LOGGER = Logger.getLogger(MinMaxSolverImpl.class.getName());
	
	private MinMaxCalc calc;

	public MinMaxSolverImpl(MinMaxCalc calc) {
		this.calc = calc;
	}

	@Override
	public double[] minMax(double[] nums) {
		
		int count = Runtime.getRuntime().availableProcessors();
		ExecutorService executor = Executors.newFixedThreadPool(count); 
		Future<MinMaxRunnable> future;
		Future<MinMaxRunnable>[] futuros = new Future[count];
		Callable<MinMaxRunnable> cb;
		double[] mins = new double[count];
		double[] maxs = new double[count];
		
		int from = 0;
		int part = nums.length / count;
		
		for (int i = 0; i < count; i++) {			
			int to = i+1 == count ? nums.length : from + part;
			LOGGER.info("from " + from + " to " + to);
			cb = new MinMaxRunnable(calc, nums, count, count);
			future = executor.submit(cb);
			futuros[i] = future;
			from = to;
		}
		
		for (int i = 0; i < count; i++) {
			try {
				mins[i] = futuros[i].get().min;
				maxs[i] = futuros[i].get().max;
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
		executor.shutdown();
		double min = calc.min(mins, 0, count);
		double max = calc.max(maxs, 0, count);
		return new double[] {min, max};
	}

	static class MinMaxRunnable implements Callable<MinMaxRunnable> {
		
		private MinMaxCalc calc;
		private double[] nums;
		private int from, to;
		private double min, max;

		public MinMaxRunnable(MinMaxCalc calc, double[] nums, int from, int to) {
			this.calc = calc;
			this.nums = nums;
			this.from = from;
			this.to = to;
		}

		public double getMin() { return min; }
		public double getMax() { return max; }

		@Override
		public MinMaxRunnable call() throws Exception {
			min = calc.min(nums, from, to);
			max = calc.max(nums, from, to);	
			return this;
		}
	}

}
