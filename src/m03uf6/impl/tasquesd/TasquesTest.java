package m03uf6.impl.tasquesd;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import lib.LoggingConfigurator;
import lib.Problems;
import lib.Reflections;
import lib.db.ConnectionFactory;
import lib.db.NotFoundException;
import m03uf6.prob.tasquesd.TascaTO;
import m03uf6.prob.tasquesd.TasquesDAO;

public class TasquesTest {
	
	static String packageName;
	static ConnectionFactory cf;
	static TasquesDAO dao;
	
	@BeforeAll
	static void beforeAll() {
		
		LoggingConfigurator.configure(Level.INFO);

		packageName = Problems.getImplPackage(TasquesDAO.class);

		// cf = new ConnectionFactoryImpl()
		cf = Reflections.newInstanceOfType(
				ConnectionFactory.class, 
				packageName + ".ConnectionFactoryImpl");
		
		// dao = new TasquesDAOImpl(cf)		
		dao = Reflections.newInstanceOfType(
				TasquesDAO.class, packageName + ".TasquesDAOImpl", 
				new Class[] {ConnectionFactory.class}, 
				new Object[] {cf});
	}
	
	@BeforeEach
	void beforeEach() {		
		dao.init(false);
	}
	
	@Test
	void checkClear() {
		
		assertEquals(0, dao.findAll().size());
	}
	
	@Test
	void createAndFind() {
		
		TascaTO tasca = getSample1();		
		int id = dao.create(tasca);
		assertEquals(tasca.id, id);
		
		try {
			dao.find(tasca.id);
		} catch (NotFoundException e) {
			fail("not found " + tasca.id);
		}
	}
		
	@Test
	void findNotFound() {
		
		try {
			dao.find(1);
			fail("should not find!");
			
		} catch (NotFoundException e) {}
	}
	
	@Test
	void createAndDelete() {
		
		TascaTO tasca = getSample1();		
		int id = dao.create(tasca);
		assertEquals(tasca.id, id);
		
		try {
			dao.delete(tasca.id);						
		} catch (NotFoundException e) {
			fail(e);
		}
		
		try {
			dao.find(tasca.id);
			fail();
			
		} catch (NotFoundException e) {}
	}
	
	@Test
	void deleteNotFound() {
		
		try {
			dao.delete(1);
			fail("should not delete!");
			
		} catch (NotFoundException e) {}
	}
	
	@Test
	void createAndUpdate() {
		
		TascaTO tasca = getSample1();		
		int id = dao.create(tasca);
		assertEquals(tasca.id, id);
		
		TascaTO tascam = getSample2();
		tascam.id = tasca.id;
		
		try {
			dao.update(tascam);						
		} catch (NotFoundException e) {
			fail(e);
		}
		
		try {
			TascaTO tasca2 = dao.find(tasca.id);
			checkEqual(tascam, tasca2);
			
		} catch (NotFoundException e) {
			fail(e);
		}
	}
	
	@Test
	void updateNotFound() {
		
		TascaTO tasca = getSample1();
		
		try {
			dao.update(tasca);
			fail("should not update!");
			
		} catch (NotFoundException e) {}
	}	
	
	@Test
	void createsAndFindAll() {
		
		dao.create(getSample1());
		dao.create(getSample2());
		dao.create(getSample3());
		
		List<TascaTO> tasques = dao.findAll();		
		assertEquals(3, tasques.size());
		
		for (TascaTO tasca: tasques) {
			switch (tasca.descripcio) {
			case DESC0:
				checkEqual(tasca, getSample1());
				break;
			case DESC1:
				checkEqual(tasca, getSample2());
				break;
			case DESC2:
				checkEqual(tasca, getSample3());
				break;
			}
		}
	}
	
	// AUXILIAR
	
	static final String DESC0 = "primera";
	static final String DESC1 = "segona";
	static final String DESC2 = "tercera";	
	
	void checkEqual(TascaTO tasca1, TascaTO tasca2) {
		
		assertEquals(tasca1.descripcio, tasca2.descripcio);
		assertEquals(tasca1.dataInici, tasca2.dataInici);
		assertEquals(tasca1.dataFinal, tasca2.dataFinal);
		assertEquals(tasca1.finalitzada, tasca2.finalitzada);
	}
	
	TascaTO getSample1() {
		
		TascaTO tasca = new TascaTO();
		tasca.descripcio = DESC0;		
		tasca.dataInici = java.sql.Date.valueOf(LocalDate.of(2020, 1, 1));
		tasca.dataFinal = java.sql.Date.valueOf(LocalDate.of(2020, 1, 31));
		tasca.finalitzada = true;
		
		return tasca;
	}
	
	TascaTO getSample2() {
		
		TascaTO tasca = new TascaTO();
		tasca.descripcio = DESC1;		
		tasca.dataInici = java.sql.Date.valueOf(LocalDate.of(2021, 2, 1));
		tasca.dataFinal = java.sql.Date.valueOf(LocalDate.of(2021, 2, 28));
		tasca.finalitzada = false;
		
		return tasca;
	}
	
	TascaTO getSample3() {
		
		TascaTO tasca = new TascaTO();
		tasca.descripcio = DESC2;		
		tasca.dataInici = java.sql.Date.valueOf(LocalDate.of(2019, 3, 1));
		tasca.dataFinal = java.sql.Date.valueOf(LocalDate.of(2019, 3, 31));
		tasca.finalitzada = true;
		
		return tasca;
	}
}
