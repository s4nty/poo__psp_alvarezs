package m03uf6.impl.tasquesd;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import lib.db.ConnectionFactory;
import lib.db.NotFoundException;
import lib.db.ScriptRunner;
import m03uf6.prob.tasquesd.TascaTO;
import m03uf6.prob.tasquesd.TasquesDAO;

public class TasquesDAOImpl implements TasquesDAO {
	private ConnectionFactory cf;

	public TasquesDAOImpl(ConnectionFactory cf) {
		this.cf = cf;
	}

	@Override
	public TascaTO find(Integer id) throws NotFoundException {
		try (Connection conn = cf.getConnection()) {
			try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM tasques WHERE id = ?")) {
				TascaTO tasca = null;
				stmt.setInt(1, id);
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					tasca = new TascaTO();
					tasca.descripcio = rs.getString("descripcio");
					tasca.id = rs.getInt("id");
					tasca.dataInici = rs.getDate("data_inici");
					tasca.dataFinal = rs.getDate("data_final");
					tasca.finalitzada = rs.getBoolean("finalitzada");
				}
				if (tasca != null) return tasca;
				else throw new NotFoundException("El registro no existe");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void delete(Integer id) throws NotFoundException {
		if (this.find(id) != null) {
			try (Connection conn = cf.getConnection()) {
				try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM tasques WHERE id = ?")) {
					stmt.setInt(1, id);
					stmt.executeUpdate();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			throw new NotFoundException("El registro no existe");
		}
	}

	@Override
	public Integer create(TascaTO t) {
		try (Connection conn = cf.getConnection()) {
			try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO tasques (descripcio, data_inici, data_final, finalitzada) VALUES (?,?,?,?)")) {
				stmt.setString(1, t.descripcio);
				stmt.setDate(2, (Date) t.dataInici);
				stmt.setDate(3, (Date) t.dataFinal);
				stmt.setBoolean(4, t.finalitzada);
				return t.id = stmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void update(TascaTO t) throws NotFoundException {
		if (this.find(t.id) != null) {
			try (Connection conn = cf.getConnection()) {
				try (PreparedStatement stmt = conn.prepareStatement("UPDATE tasques SET descripcio = ?, data_inici = ?, data_final = ?, finalitzada = ? WHERE id = ?")) {
					stmt.setString(1, t.descripcio);
					stmt.setDate(2, (Date) t.dataInici);
					stmt.setDate(3, (Date) t.dataFinal);
					stmt.setBoolean(4, t.finalitzada);
					stmt.setInt(5, t.id);
					stmt.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} else {
			throw new NotFoundException("El registro no existe");
		}
	}

	@Override
	public List<TascaTO> findAll() {
		List<TascaTO> list = new ArrayList<>();
		try (Connection conn = cf.getConnection(); Statement statement = conn.createStatement(); ResultSet result = statement.executeQuery("SELECT * FROM tasques")) {
			while (result.next()) {
				TascaTO tasca = null;
				try {
					tasca = this.find(result.getInt("id"));
				} catch (NotFoundException e) {
					e.printStackTrace();
				}
				if(tasca != null) list.add(tasca);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public void init(boolean withData) {
		ScriptRunner sr = new ScriptRunner(cf);
		List<String> comandes = new ArrayList<>();
		comandes.add("DROP TABLE IF EXISTS `tasques`;\n");
		comandes.add("CREATE TABLE `tasques` ( " + "`id` int NOT NULL AUTO_INCREMENT, "
				+ "`descripcio` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL, "
				+ "`data_inici` datetime DEFAULT NULL, " + "`data_final` datetime NOT NULL, "
				+ "`finalitzada` tinyint DEFAULT '0', " + "PRIMARY KEY (`id`) " + ") ENGINE=InnoDB;\n");
		if (withData)
			comandes.add("INSERT INTO `tasques` (descripcio, data_inici, data_final, finalitzada) VALUES "
					+ "('viatjar','2020-10-10 20:00:00','2020-10-20 00:00:00',0),"
					+ "('viure','2020-07-01 00:00:00','2020-10-01 00:00:00',1),"
					+ "('dormir','2020-12-21 00:00:00','2021-01-07 00:00:00',0);\n");
		sr.run(comandes);
	}
}
