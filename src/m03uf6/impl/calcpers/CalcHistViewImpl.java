package m03uf6.impl.calcpers;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class CalcHistViewImpl implements CalcHistView {

	private CalcHistPresenter p;
	private Label label;
	private TextField text;
	private Button enrBtn, endBtn, clrBtn;

	public CalcHistViewImpl(Stage stage) {
		initStage(stage);
	}
	
	private void initStage(Stage stage) {
		
		VBox vb = new VBox(10);
		
		Font font = new Font(20);
		
		text = new TextField();
		text.setFont(font);
		
		label = new Label();
		label.setFont(font);
		
		Button calcBtn = new Button("Calcula");
		calcBtn.setFont(font);
		calcBtn.setOnAction(event -> p.calculaClic(text.getText()));
		
		enrBtn = new Button("<");
		enrBtn.setFont(font);
		enrBtn.setOnAction(event ->p.enrereClic());
		
		endBtn = new Button(">");
		endBtn.setFont(font);
		endBtn.setOnAction(event ->p.endavantClic());
		
		clrBtn = new Button("Neteja");
		clrBtn.setFont(font);
		clrBtn.setOnAction(event -> p.netejaClic());
		
		HBox btnsPane = new HBox();
		btnsPane.getChildren().addAll(calcBtn, enrBtn, endBtn, clrBtn);
		
		vb.getChildren().addAll(text, label, btnsPane);
		
		Scene scene = new Scene(vb, 400, 200);
		stage.setScene(scene);
		stage.setTitle("Calculadora Santy | Persistent");
		stage.show();
	}
	
	@Override
	public void setPresenter(CalcHistPresenter p) {
		this.p = p;
	}

	@Override
	public void setMostra(String display) {
		label.setText(display);
	}

	@Override
	public void setInput(String display) {
		text.setText(display);
	}

	@Override
	public void mostraEnrere(boolean b) {
		enrBtn.setDisable(!b);
	}

	@Override
	public void mostraEndavant(boolean b) {
		endBtn.setDisable(!b);
	}
	
	@Override
	public void mostraNeteja(boolean b) {
		clrBtn.setDisable(!b);
	}
	
	public void requestFocus() {
		text.requestFocus();
	}
}