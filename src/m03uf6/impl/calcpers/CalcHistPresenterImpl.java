package m03uf6.impl.calcpers;

import java.io.File;
import java.io.IOException;

import org.json.JSONObject;

import lib.TextFileStorage;
import m03uf5.impl.calcbox.ExprAvaluator2;
import m03uf6.impl.calcpers.CalcHistoric.JSONSerial;

public class CalcHistPresenterImpl implements CalcHistPresenter {

	private CalcHistoric h;	
	private ExprAvaluator2 ea;
	private CalcHistView v;
	private TextFileStorage tf;
	private JSONSerial json;
	
	public CalcHistPresenterImpl() {
		h = new CalcHistoric();
		ea = new ExprAvaluator2();
		tf = new TextFileStorage();
		json = new JSONSerial();
		if(getFile()) {
			try {
				String s = tf.readFromFile("calc.json");
				JSONObject j = new JSONObject(s);
				h = json.deserialize(j);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				new File("data/calc.json").createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void setView(CalcHistView v) {
		this.v = v;
		if(h.size() > 0) {
			v.setInput(h.current());
			calculaIMostra(h.current(), false);
		}
		v.mostraEndavant((h.size() == h.index()+1) ? false : true);
		v.mostraEnrere((h.size() == 0 || h.index() == 0) ? false : true);
		v.mostraNeteja((h.size() == 0 || (h.index() == 0 && h.size() == 0)) ? false : true);
		
	}

	@Override
	public void calculaClic(String expr) {
		calculaIMostra(expr, true);
		v.requestFocus();
		v.setInput("");
		this.save();
	}

	@Override
	public void netejaClic() {
		h.clear();
		v.mostraEnrere(false);
		v.mostraEndavant(false);	
		v.mostraNeteja(false);
		v.setMostra("");
		this.save();
	}

	@Override
	public void enrereClic() {		
		h.prev();
		calculaIMostra(h.current(), false);
		this.save();
	}

	@Override
	public void endavantClic() {			
		h.next();
		calculaIMostra(h.current(), false);
		this.save();
	}
	
	public void save() {
		try {
			tf.writeToFile("calc.json", h.toString());
			json.serialize(h);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private boolean getFile()  {
		return new File("data/calc.json").exists();
	}
	
	private void calculaIMostra(String expr, boolean afegir) {
	
		double valor;
		try {
			valor = ea.avalua(expr);
			if (afegir) {
				h.add(expr);
			}			
		} catch (Exception e) {			
			v.setMostra("error!");
			return;
		}
				
		v.setInput(expr);
		v.setMostra(Double.toString(valor));
		v.mostraEnrere(h.index() > 0);
		v.mostraEndavant(h.index() + 1 < h.size());	
		v.mostraNeteja(h.size() > 0);
	}
	
	
}