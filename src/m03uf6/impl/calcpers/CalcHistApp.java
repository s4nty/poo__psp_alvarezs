package m03uf6.impl.calcpers;

import javafx.application.Application;
import javafx.stage.Stage;

public class CalcHistApp extends Application {
	CalcHistPresenterImpl p;

	@Override
	public void start(Stage primaryStage) throws Exception {

		CalcHistView v = new CalcHistViewImpl(primaryStage);
		p = new CalcHistPresenterImpl();

		p.setView(v);
		v.setPresenter(p);
	}
	// Esta funci�n no la utilizo ya que guardo automaticamente cada vez que se le da al boton de calcular o netejar
	/*@Override
	public void stop() {
		p.save();
	}*/

	public static void main(String[] args) {
		launch(args);
	}

}

interface CalcHistView {

	void setPresenter(CalcHistPresenter p);

	void setInput(String display);

	void setMostra(String display);

	void mostraEnrere(boolean b);

	void mostraEndavant(boolean b);

	void mostraNeteja(boolean b);

	void requestFocus();
}

interface CalcHistPresenter {

	void setView(CalcHistView v);

	void calculaClic(String text);

	void enrereClic();

	void endavantClic();

	void netejaClic();
}