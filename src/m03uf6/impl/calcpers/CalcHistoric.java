package m03uf6.impl.calcpers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import lib.JSONSerializer;

public class CalcHistoric implements Iterable<String> {

	private List<String> historic;
	private int idx;
	
	public CalcHistoric() {
		this(new ArrayList<>(), -1);
	}
	
	public CalcHistoric(List<String> exps, int idx) {
		this.historic = exps;
		this.idx = idx;
	}
	
	public void add(String exp) {
		historic.add(exp);
		idx = historic.size() - 1;
	}
	
	public int size() {
		return historic.size();
	}
	
	public int index() {
		return idx;
	}
	
	@Override
	public Iterator<String> iterator() {
		return historic.iterator();
	}
	
	public void prev() {		
		if (idx < 1) {
			throw new IllegalArgumentException("enrere?");
		}
		
		idx --;
	}
	
	public void next() {
		if (idx + 1 == historic.size()) {
			throw new IllegalArgumentException("endavant?");
		}		
		idx ++;
	}
	
	public String current() {
		if (idx == -1) {
			return null;
		}
		
		return historic.get(idx);
	}
	
	public void clear() {
		historic.clear();
		idx = - 1;
	}
	
	public List<String> getExpr() {
		return historic;
	}
	
	public String toString() {
		return "{\n\t\"expressions\": \n\t" + historic.toString() + "\n\t,\n\t\"index\": " + idx + "\n}";
	}
	public static class JSONSerial implements JSONSerializer<CalcHistoric> {

		@Override
		public CalcHistoric deserialize(JSONObject json) {
			JSONArray j = json.getJSONArray("expressions");
			List<String> js = new ArrayList<String>();
			for(int i = 0; i < j.length(); i++) {
				js.add(j.getString(i));
			}
			return new CalcHistoric(js, json.getInt("index"));
		}

		@Override
		public JSONObject serialize(CalcHistoric t) {
			return new JSONObject().put("expressions", t.getExpr()).put("index", t.index());
		}
		
	}
}
