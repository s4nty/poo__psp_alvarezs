package m03uf6.impl.followb;

import java.util.logging.Level;
import java.util.logging.Logger;

import lib.LoggingConfigurator;
import lib.Problems;
import lib.Reflections;
import lib.db.ConnectionFactory;
import lib.db.DuplicateException;
import lib.db.NotFoundException;
import m03uf6.prob.followb.FollowDAO;

public class FollowRun {

	static final Logger LOGGER = Logger.getLogger(FollowRun.class.getName());	
	
	public static void main(String[] args) {
		
		LoggingConfigurator.configure(Level.INFO);

		String packageName = Problems.getImplPackage(FollowDAO.class);

		// cf = new ConnectionFactoryImpl()
		ConnectionFactory cf = Reflections.newInstanceOfType(
				ConnectionFactory.class, 
				packageName + ".ConnectionFactoryImpl");
		
		// dao = new FollowDAOImpl(cf)		
		FollowDAO dao = Reflections.newInstanceOfType(
				FollowDAO.class, packageName + ".FollowDAOImpl", 
				new Class[] {ConnectionFactory.class}, 
				new Object[] {cf});
		
		try {
			dao.init();
			
			dao.crear("maria");
			dao.crear("pere");
			dao.crear("joan");
			
			boolean creats = dao.existeix("maria") && dao.existeix("pere") && dao.existeix("joan");			
			LOGGER.info("==> 3 usuaris creats: " + creats);
			
			dao.seguir("pere", "maria");
			dao.seguir("joan", "pere");
			dao.seguir("joan", "maria");
			
			LOGGER.info("==> pere-->maria, joan-->pere, joan-->maria, follows: " + countFollows(dao));
			printUsers(dao, "maria", "pere", "joan");
			
			dao.deixarDeSeguir("pere", "maria");
			
			LOGGER.info("==> pere unfollow de maria, follows: " + countFollows(dao));
			printUsers(dao, "maria", "pere", "joan");
			
			dao.esborrar("maria");
			LOGGER.info("==> maria esborrada, follows: " + countFollows(dao));
			printUsers(dao, "joan", "pere");
			
			dao.esborrar("joan");			
			LOGGER.info("==> joan esborrat, follows: " + countFollows(dao));
			printUsers(dao, "pere");
			
			dao.esborrar("pere");
			boolean queden = dao.existeix("maria") || dao.existeix("pere") || dao.existeix("joan");
			LOGGER.info("==> pere esborrat, queden: " + queden);
			
		} catch (NotFoundException | DuplicateException e) {
			LOGGER.log(Level.SEVERE, "hi ha hagut un problema", e);
		}
	}
	
	static int countFollows(FollowDAO dao) {
		
		int count = 0;
		for (String user: new String[]{"maria", "pere", "joan"}) {
			count += dao.getSeguits(user).size();
		}
		return count;
	}
	
	static void printUsers(FollowDAO dao, String... users) {
		
		for (String user: users) {
			LOGGER.info(user + " followers: " + dao.getSeguidors(user) 
				+ ", following: " + dao.getSeguits(user));	
		}
	}
}
