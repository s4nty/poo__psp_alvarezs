package m03uf6.impl.followb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.HashSet;
import java.util.Set;

import lib.db.ConnectionFactory;
import lib.db.DAOException;
import lib.db.DuplicateException;
import lib.db.NotFoundException;
import m03uf6.prob.followb.FollowDAO;
import m03uf6.prob.followb.FollowDB;

public class FollowDAOImpl implements FollowDAO {
	private ConnectionFactory cf;

	public FollowDAOImpl(ConnectionFactory cf) {
		this.cf = cf;
	}

	@Override
	public void init() {
		FollowDB.init(cf);
	}

	@Override
	public boolean existeix(String usuari) {
		boolean v = false;
		try (Connection conn = cf.getConnection()) {
			try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM usuaris WHERE nom = ?")) {
				stmt.setString(1, usuari);
				ResultSet rs = stmt.executeQuery();
				if (rs.next()) {
					v = true;
				}
				return v;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return v;
	}

	@Override
	public void crear(String usuari) throws DuplicateException {
		try (Connection conn = cf.getConnection();
				PreparedStatement stmt = conn.prepareStatement("INSERT INTO usuaris VALUES (?)")) {
			stmt.setString(1, usuari);
			stmt.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new DuplicateException("El usuario ya existe");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void esborrar(String usuari) throws NotFoundException {
		try (Connection conn = cf.getConnection()) {
			conn.setAutoCommit(false);
			try {
				if(!this.existeix(usuari)) {
					throw new NotFoundException("El usuario no existe");
				}
				this.borrarDatosFollows(usuari, conn);
				this.borrarUsuario(usuari, conn);
				conn.commit();
			} catch (Exception e) {
				conn.rollback();
				throw e;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void seguir(String seguidor, String seguit) throws NotFoundException {
		try (Connection conn = cf.getConnection();
				PreparedStatement stmt = conn.prepareStatement("INSERT INTO seguidors VALUES (?, ?)")) {
			stmt.setString(1, seguit);
			stmt.setString(2, seguidor);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new NotFoundException(e);
		}
	}

	@Override
	public void deixarDeSeguir(String seguidor, String seguit) throws NotFoundException {
		try (Connection conn = cf.getConnection();
				PreparedStatement stmt = conn.prepareStatement("DELETE FROM seguidors WHERE seguit = ? AND seguidor = ?")) {
			stmt.setString(1, seguit);
			stmt.setString(2, seguidor);
			if(stmt.executeUpdate() == 0) {
				throw new NotFoundException("No lo sigue"); 
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Set<String> getSeguidors(String usuari) {
		Set<String> list = new HashSet<String>();
		try (Connection conn = cf.getConnection();
				PreparedStatement stmt = conn.prepareStatement("SELECT seguidor FROM seguidors WHERE seguit = ?")) {
			stmt.setString(1, usuari);
			try (ResultSet rs = stmt.executeQuery()) {
				while(rs.next()) {
					list.add(rs.getString(1));
				}
				return list;
			}	
		} catch (SQLException e) {
			e.printStackTrace();;
		}
		return list;
	}

	@Override
	public Set<String> getSeguits(String usuari) {
		Set<String> list = new HashSet<String>();
		try (Connection conn = cf.getConnection();
				PreparedStatement stmt = conn.prepareStatement("SELECT seguit FROM seguidors WHERE seguidor = ?")) {
			stmt.setString(1, usuari);
			try (ResultSet rs = stmt.executeQuery()) {
				while(rs.next()) {
					list.add(rs.getString(1));
				}
				return list;
			}	
		} catch (SQLException e) {
			e.printStackTrace();;
		}
		return list;
	}

	/* FUNCIONES PRIVADAS */

	private void borrarDatosFollows(String usuari, Connection conn) throws NotFoundException {
		try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM seguidors WHERE seguit = ? OR seguidor = ?")) {
			stmt.setString(1, usuari);
			stmt.setString(2, usuari);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new NotFoundException(e);
		}
	}

	private void borrarUsuario(String usuari, Connection conn) {
		try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM usuaris WHERE nom = ?")) {
			stmt.setString(1, usuari);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


}
