package m03uf6.impl.followb;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Set;
import java.util.logging.Level;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import lib.LoggingConfigurator;
import lib.Problems;
import lib.Reflections;
import lib.db.ConnectionFactory;
import lib.db.DuplicateException;
import lib.db.NotFoundException;
import m03uf6.prob.followb.FollowDAO;

public class FollowTest {
	
	static String packageName;
	static ConnectionFactory cf;
	static FollowDAO dao;

	@BeforeAll
	static void beforeAll() {
		
		LoggingConfigurator.configure(Level.INFO);

		packageName = Problems.getImplPackage(FollowDAO.class);

		// cf = new ConnectionFactoryImpl()
		cf = Reflections.newInstanceOfType(
				ConnectionFactory.class, 
				packageName + ".ConnectionFactoryImpl");
		
		// dao = new FollowDAOImpl(cf)		
		dao = Reflections.newInstanceOfType(
				FollowDAO.class, packageName + ".FollowDAOImpl", 
				new Class[] {ConnectionFactory.class}, 
				new Object[] {cf});
	}	
	
	@BeforeEach
	void beforeEach() {		
		
		dao.init();
	}
	
	@Test
	void testDuplicate() throws DuplicateException {
		
		dao.crear("pere");		
		assertTrue(dao.existeix("pere"));
		
		try {
			dao.crear("pere");
			fail("it should fail");
			
		} catch (DuplicateException e) {}
	}
	
	@Test
	void testFollowNotFound() {
		
		try {
			dao.seguir("maria", "joan");
			fail("it should fail");
			
		} catch (NotFoundException e) {}		
	}
	
	@Test
	void testUnfollowNotFound() {

		try {
			dao.deixarDeSeguir("maria", "joan");
			fail("it should fail");
			
		} catch (NotFoundException e) {}
	}
	
	@Test
	void testDeleteNotFound() {

		try {
			dao.esborrar("none");
			fail("it should fail");
			
		} catch (NotFoundException e) {}
	}
	
	@Test
	void testRepeatNotFound() throws DuplicateException, NotFoundException {
				
		dao.crear("maria");
		dao.crear("pere");
		dao.crear("joan");
		
		dao.seguir("pere", "maria");
		
		try {
			dao.seguir("pere", "maria");
			fail("it should fail");
			
		} catch (NotFoundException e) {}
		
		dao.deixarDeSeguir("pere", "maria");
		
		try {
			dao.deixarDeSeguir("pere", "maria");
			fail("it should fail");
			
		} catch (NotFoundException e) {}
	}
	
	@Test
	void testCreateDelete() throws DuplicateException, NotFoundException {
		
		assertFalse(dao.existeix("none"));
		
		dao.crear("maria");
		assertTrue(dao.existeix("maria"));
		dao.crear("pere");
		assertTrue(dao.existeix("pere"));
		dao.crear("joan");
		assertTrue(dao.existeix("joan"));
		
		dao.esborrar("maria");
		assertFalse(dao.existeix("maria"));
		dao.esborrar("pere");
		assertFalse(dao.existeix("pere"));
		dao.esborrar("joan");
		assertFalse(dao.existeix("joan"));
	}
	
	@Test
	void testFollow() throws NotFoundException, DuplicateException {
		
		dao.crear("maria");
		dao.crear("pere");
		dao.crear("joan");
		
		dao.seguir("pere", "maria");
		dao.seguir("joan", "pere");
		dao.seguir("joan", "maria");
				
		Set<String> names;
		
		names = dao.getSeguidors("maria");
		assertEquals(2, names.size());
		assertTrue(names.contains("pere"));
		assertTrue(names.contains("joan"));		
		
		names = dao.getSeguits("pere");
		assertEquals(1, names.size());
		assertTrue(names.contains("maria"));
		
		dao.deixarDeSeguir("pere", "maria");
		
		names = dao.getSeguidors("maria");
		assertEquals(1, names.size());
		assertTrue(names.contains("joan"));
		
		names = dao.getSeguits("pere");
		assertEquals(0, names.size());
		
		dao.esborrar("maria");
		dao.esborrar("joan");
		
		assertEquals(0, dao.getSeguits("pere").size());
		assertEquals(0, dao.getSeguidors("pere").size());
		
		dao.esborrar("pere");
	}
}
