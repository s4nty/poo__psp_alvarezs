package m03uf6.impl.banc;

import java.sql.Connection;
import java.sql.SQLException;

import lib.db.ConnectionFactory;
import m03uf6.prob.tasques1.TasquesConnect;

public class ConnectionFactoryImpl implements ConnectionFactory {
	private ConnectionFactory cf;
	
	public ConnectionFactoryImpl() {
		cf = TasquesConnect.getDataSourceFactory();
	}

	@Override
	public Connection getConnection() throws SQLException {
		return cf.getConnection();
	}

}