package m03uf6.impl.banc;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import lib.db.ConnectionFactory;
import lib.db.DuplicateException;
import lib.db.NotFoundException;
import m03uf6.prob.banc.BancDAO;
import m03uf6.prob.banc.BancDB;
import m03uf6.prob.banc.Moviment;
import m03uf6.prob.banc.SenseFonsException;

public class BancDAOImpl implements BancDAO {
	private ConnectionFactory cf;

	public BancDAOImpl(ConnectionFactory cf) {
		this.cf = cf;
	}

	@Override
	public void init() {
		BancDB.init(cf);
	}

	@Override
	public void nouCompte(int numero, BigDecimal saldoInicial) throws DuplicateException {
		try {
			if (!this.find(numero)) {
				try (Connection conn = cf.getConnection()) {
					try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO compte (id_compte, saldo) VALUES (?,?)")) {
						stmt.setInt(1, numero);
						stmt.setFloat(2, saldoInicial.floatValue());
						stmt.executeUpdate();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				throw new DuplicateException("La cuenta ya existe");
			}
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int transferir(int origen, int desti, BigDecimal quantitat) throws NotFoundException, SenseFonsException {
		if (this.find(origen) && this.find(desti)) {
			if(this.getSaldo(origen).compareTo(quantitat) >= 0) {
				this.actualitzarCompte(origen, quantitat, true);
				this.actualitzarCompte(desti, quantitat, false);
				try (Connection conn = cf.getConnection()) {
					try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO moviment (id_compte_origen, id_compte_desti, quantitat) VALUES (?,?,?)")) {
						stmt.setInt(1, origen);
						stmt.setInt(2, desti);
						stmt.setFloat(3, quantitat.floatValue());
						return stmt.executeUpdate();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				throw new SenseFonsException("La cuenta no tiene saldo suficiente");
			}
			return 0;
		} else {
			throw new NotFoundException("Alguna de las cuentas no existe");
		}
	}

	@Override
	public List<Moviment> getMoviments(int origen) {
		List<Moviment> list = new ArrayList<>();
		try (Connection conn = cf.getConnection();PreparedStatement stmt = conn.prepareStatement("SELECT * FROM `moviment` WHERE id_compte_origen = ? OR id_compte_desti = ? ORDER BY id_moviment ASC")) {
			stmt.setInt(1, origen);
			stmt.setInt(2, origen);
			ResultSet result = stmt.executeQuery();
			while (result.next()) {
				Moviment m = new Moviment();
				m.origen = result.getInt("id_compte_origen");
				m.desti = result.getInt("id_compte_desti");
				m.quantitat = result.getBigDecimal("quantitat");
				list.add(m);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public BigDecimal getSaldo(int compte) throws NotFoundException {
		if(this.find(compte)) {
			try (Connection conn = cf.getConnection()) {
				try (PreparedStatement stmt = conn.prepareStatement("SELECT saldo FROM compte WHERE id_compte = ?")) {
					stmt.setInt(1, compte);
					ResultSet rs = stmt.executeQuery();
					if (rs.next()) {
						return rs.getBigDecimal("saldo");
					} else {
						throw new NotFoundException("La cuenta no existe");
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			throw new NotFoundException("La cuenta no existe");
		}
		return null; 
	}

	/* FUNCIONES PRIVADAS */

	private boolean find(Integer id) throws NotFoundException {
		boolean v = false;
		try (Connection conn = cf.getConnection()) {
			try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM compte WHERE id_compte = ?")) {
				stmt.setInt(1, id);
				ResultSet rs = stmt.executeQuery();
				if (rs.next()) {
					v = true;
				}
				return v;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return v;
	}
	
	private void actualitzarCompte(Integer compte, BigDecimal quantitat, boolean restar) {
		try (Connection conn = cf.getConnection()) {
			try (PreparedStatement stmt = conn.prepareStatement("UPDATE compte SET saldo = ? WHERE id_compte = ?")) {
				BigDecimal totalSaldo = this.getSaldo(compte);
				stmt.setFloat(1, (restar) ? totalSaldo.subtract(quantitat).floatValue() : totalSaldo.add(quantitat).floatValue());
				stmt.setInt(2, compte);
				stmt.executeUpdate();
			} catch (NotFoundException e) {
				e.printStackTrace();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}