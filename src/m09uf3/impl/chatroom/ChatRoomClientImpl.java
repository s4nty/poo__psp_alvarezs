package m09uf3.impl.chatroom;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import m09uf3.prob.chatroom.ChatRoomClient;
import m09uf3.prob.chatroom.ChatRoomHandler;

public class ChatRoomClientImpl implements ChatRoomClient, Runnable {
	private String host;
	private int port;
	private int operacion;
	private int size;
	private Thread thread;
	private boolean connected;
	DatagramSocket socket;
	private InetAddress address;
	private DatagramPacket packetU;
	//private ArrayList<Users> usuarios = new ArrayList<Users>();
	static final Logger LOGGER = Logger.getLogger(ChatRoomClientImpl.class.getName());
	static final Charset UTF8 = StandardCharsets.UTF_8;
	static final Charset ISO8859_1 = StandardCharsets.ISO_8859_1;
	static final Charset CHARSET = UTF8;
	static final int TIMEOUT_MILLIS = 1500;
	static final int SIZE = 512;
	public ChatRoomClientImpl(String host, int port) {
		this.host = host;
		this.port = port;
	}
	@Override
	public void connect(String username, ChatRoomHandler handler) throws IOException {
		this.socket = new DatagramSocket();
		this.socket.setSoTimeout(TIMEOUT_MILLIS);
		this.address = InetAddress.getByName(host);
		this.connected = true;
		this.thread = new Thread(this, "ChatRoomClient");
		this.thread.start();
		this.request(socket, address, port, this.msgClient(username, 1));
	}

	@Override
	public void message(String message) throws IOException {
		this.request(socket, address, port, this.msgClient(message, 2));
	}

	@Override
	public void disconnect() throws IOException {
		this.connected = false;
		this.request(socket, address, port, msgClient("", 3));
		this.socket.disconnect();
	}
	private void request(DatagramSocket socket, InetAddress address, int port, String text) 
			throws IOException {
		int longitud = text.getBytes(CHARSET).length + 2;
		byte[] msj = new byte[longitud];
		msj[0] = (byte) this.operacion;
		msj[1] = (byte) this.size;
		byte[] buf = text.getBytes(CHARSET);
		for(int i = 0; i < text.getBytes(CHARSET).length; i++) {
			msj[i+2] = buf[i];
		}
		this.packetU = new DatagramPacket(msj, longitud, address, port);
		socket.send(packetU);		
	}
	private String msgClient(String msg, int operacion) {
		this.operacion = operacion;
		this.size = msg.getBytes(CHARSET).length;
		return msg;
	}
	@Override
	public void run() {
		while(this.connected) {
			try {
				DatagramPacket packet = new DatagramPacket(new byte[SIZE], SIZE);
				this.socket.receive(packet);
				byte[] datos = packet.getData();
				int usuarioL = datos[1];
				String user = new String(datos, 2, usuarioL, CHARSET);
				int mensajeL = datos[usuarioL + 2];
				String mensaje = new String(datos, 3+usuarioL, mensajeL, CHARSET);
				LOGGER.info(user + ": " + mensaje);				
			} catch (IOException e) {
			}
		}
	}

}
