package m09uf3.impl.chatroom;

import java.net.InetAddress;

public class Users {
	private String user;
	private InetAddress host;
	private int port;
	
	public Users(String user, InetAddress host, int port) {
		this.user = user;
		this.host = host;
		this.port = port;
	}
	
	public String getUser() {
		return this.user;
	}
	
	public InetAddress getHost() {
		return this.host;
	}
	
	public int getPort() {
		return this.port;
	}
}
