package m09uf3.impl.chatroom;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import lib.Startable;

public class ChatRoomServerImpl implements Startable, Runnable {
	private String host;
	private int port;
	private volatile boolean connected;
	private Thread thread;
	private DatagramSocket socket;
	private DatagramPacket packet;
	private ArrayList<Users> usersConnected = new ArrayList<Users>();
	static final int TIMEOUT_MILLIS = 1500;
	static final int SIZE = 512;
	static final Charset UTF8 = StandardCharsets.UTF_8;
	static final Charset ISO8859_1 = StandardCharsets.ISO_8859_1;
	static final Charset CHARSET = UTF8;
	static final Logger LOGGER = Logger.getLogger(ChatRoomServerImpl.class.getName());

	public ChatRoomServerImpl(String host, int port) {
		this.host = host;
		this.port = port;
	}

	@Override
	public void start() {
		try {
			this.thread = new Thread(this, "ChatRoomServerImpl");
			this.socket = new DatagramSocket(port, InetAddress.getByName(this.host));
			this.connected = true;
			this.thread.start();
		} catch (SocketException e) {
		} catch (UnknownHostException e) {
		}
	}

	@Override
	public void stop() {
		this.connected = false;
		this.thread.interrupt();
		this.socket.close();
	}

	@Override
	public boolean isStarted() {
		return this.thread.isAlive();
	}

	@Override
	public void run() {
		while (this.connected) {
			try {
				this.socket.setSoTimeout(TIMEOUT_MILLIS);
				this.packet = new DatagramPacket(new byte[SIZE], SIZE);
				//String text = new String(packet.getData(), 0, packet.getLength(), CHARSET);
				//LOGGER.info("received " + packet.getLength());
				//break;
				try {
					this.socket.receive(this.packet);
					handle(socket, packet);
				} catch (SocketTimeoutException e) {
					LOGGER.fine("timeout receiving!");
				}

			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, "receiving", e);
			}
		}
	}

	private void handle(DatagramSocket socket, DatagramPacket packet) {
		decode(packet);
	}
	
	private void decode(DatagramPacket packet) {
		byte[] mensajeCompleto = packet.getData();
		InetAddress userHost = packet.getAddress();
		int userPort = packet.getPort();
		Users usuario = this.getUser(userHost, userPort);
		int opcion = mensajeCompleto[0];
		switch(opcion) {
			case 1:
				LOGGER.info("Conectado");
				int longitudU = mensajeCompleto[1];
				String user = new String(mensajeCompleto, 2, longitudU, CHARSET);
//				LOGGER.info(user);
				usersConnected.add(new Users(user, userHost, this.port));
				break;
			case 2:
				LOGGER.info("Mensaje");
				int longitudM = mensajeCompleto[1];
				String mensaje = new String(mensajeCompleto, 2, longitudM, CHARSET);
				byte[] msgToSendBytes = new byte[mensaje.length() + 2];
				msgToSendBytes[0] = (byte) 1;
				msgToSendBytes[1] = (byte) usuario.getUser().length();
				// POR HACER
				// Poner en el array msgToSendBytes el nombre de usuario, la longitud del mensaje y el mensaje, todo en bytes
				
				//msgToSendBytes[2] = mensaje.toUpperCase().getBytes(CHARSET);
				for(Users u : usersConnected) {
					//DatagramPacket msgToSend = new DatagramPacket(mensaje, mensaje.length(), u.getHost(), u.getPort());					
				}
				LOGGER.info(mensaje);
				break;
			case 3:
				LOGGER.info("Desconectado");
				this.usersConnected.remove(this.getUser(userHost, userPort));
				break;
			default:
				LOGGER.info("F");
				break;
		}
	}
	
	public Users getUser(InetAddress host, int port) {
		for(Users u : this.usersConnected) {
			if(u.getHost().equals(host) && u.getPort() == port) {
				return u;
			}
		}
		// No deber�a de llegar aqu�
		return null;
	}

}
