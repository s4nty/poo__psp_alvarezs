package m09uf3.impl.guessb;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import lib.Startable;
import m09uf3.prob.guessb.GuessSecret;

public class GuessServerImpl implements Startable, Runnable {

	static final Logger LOGGER = Logger.getLogger(GuessServerImpl.class.getName());
	static final int ACCEPT_TIMEOUT_MILLIS = 1500;
	static final int READ_TIMEOUT_MILLIS = 1500;

	private String host;
	private int port;
	private GuessSecret gs;
	private Thread thread;
	private ServerSocket serverSocket;

	public GuessServerImpl(String host, int port, GuessSecret gs) {
		this.host = host;
		this.port = port;
		this.gs = gs;
	}

	@Override
	public void start() {
		this.thread = new Thread(this, "server");
		this.thread.start();
	}

	@Override
	public void stop() {
		try {
			if (serverSocket != null)
				serverSocket.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean isStarted() {
		return thread != null && thread.isAlive();
	}

	@Override
	public void run() {

		ExecutorService service = Executors.newCachedThreadPool();

		LOGGER.info("starting server at " + host + ":" + port);

		try {
			serverSocket = new ServerSocket(port, 10, InetAddress.getByName(host));
			serverSocket.setSoTimeout(ACCEPT_TIMEOUT_MILLIS);

			while (true) {
				try {
					Socket clientSocket = serverSocket.accept();
					clientSocket.setSoTimeout(READ_TIMEOUT_MILLIS);
					service.submit(() -> handle(clientSocket));

				} catch (SocketTimeoutException e) {
					// timeout in the accept: ignore
					LOGGER.fine("timeout accepting");
				}
			}

		} catch (IOException e) {
			if (serverSocket != null && serverSocket.isClosed()) {
				LOGGER.info("server closed");
			} else {
				LOGGER.log(Level.SEVERE, "accepting", e);
			}

		} finally {
			service.shutdown();
		}
	}

	protected void handle(Socket clientSocket) {
		try {
			boolean v = false;
			OutputStream os = clientSocket.getOutputStream();
			InputStream in = clientSocket.getInputStream();
			int secret = (int) gs.next();
			byte[] send = new byte[2];
			send[0] = 1;
			send[1] = (byte) gs.upper();
			os.write(send, 0, send.length);
			while (!v) {
				try {
					clientSocket.setSoTimeout(1500);
					byte[] buf = new byte[1];
					in.read(buf, 0, buf.length);
					int userNumber = (int) buf[0];
					if (userNumber > secret) os.write(1);
					else if (userNumber < secret) os.write(2);
					else os.write(3);
				} catch (SocketTimeoutException e) {
					continue;
				}
			}
		} catch (IOException e) {

		}

	}
}
