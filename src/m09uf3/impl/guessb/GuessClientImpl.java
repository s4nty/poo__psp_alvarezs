package m09uf3.impl.guessb;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.logging.Logger;

import m09uf3.prob.guessb.GuessClient;
import m09uf3.prob.guessb.GuessStatus;

public class GuessClientImpl implements GuessClient {
	
	static final Logger LOGGER = Logger.getLogger(GuessClientImpl.class.getName());
	static final Charset UTF8 = StandardCharsets.UTF_8;
	static final Charset ISO8859_1 = StandardCharsets.ISO_8859_1;
	static final Charset CHARSET = ISO8859_1;
	private String host;
	private int port;
	private Socket socket;
	private OutputStream os;
	private InputStream in;
	public GuessClientImpl(String host, int port) {
		this.host = host;
		this.port = port;
	}

	@Override
	public byte[] start() {
		int x = 0;
		try {
			socket = new Socket();
			socket.connect(new InetSocketAddress(this.host, this.port), 1500);
			socket.setSoTimeout(1000);
			in = socket.getInputStream();
			byte[] buf = new byte[2];
			in.read(buf, 0, buf.length);
			x = buf[1];
			
		} catch (IOException e) {
			LOGGER.info("hola");
			e.printStackTrace();
		}
		return new byte[] {1, (byte) x};
	}

	@Override
	public GuessStatus play(byte play) {
		int response = 0;
		try {
			in = socket.getInputStream();
			os = socket.getOutputStream();
			os.write(play);
			byte[] buf = new byte[1];
			in.read(buf, 0, buf.length);
			response = buf[0];
			System.out.println(Arrays.toString(buf));
		} catch (IOException e) {
			
		}
		return response == 1 ? GuessStatus.SMALLER : response == 2 ? GuessStatus.LARGER : GuessStatus.EXACT;
	}

}
