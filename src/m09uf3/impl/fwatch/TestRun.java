package m09uf3.impl.fwatch;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import lib.LoggingConfigurator;
import lib.Problems;
import lib.Reflections;
import lib.Startable;
import lib.Threads;
import m09uf3.prob.fwatch.FWatchClient;

public class TestRun {

	static final Logger LOGGER = Logger.getLogger(FWatchRun.class.getName());
	
	static final int PORT = 5510;
	
	private static FWatchClient getClientInstance(String host) {
		
		String packageName = Problems.getImplPackage(FWatchClient.class);		
		return Reflections.newInstanceOfType(
				FWatchClient.class, packageName + ".FWatchClientImpl",
				new Class[] {String.class, int.class}, 
				new Object[] {host, PORT});
	}
	
	public static void main(String[] args) {
		
		LoggingConfigurator.configure(Level.INFO);
				
		//Startable server = "10.1.5.110";		
		//server.start();		
		Threads.sleep(500); // some time to start server
		
		FWatchClient client = getClientInstance("10.1.5.10");
		
		// assumes the server is in the same machine/OS
		String folder = System.getProperty("user.home") + File.separator + "fwatch";
		try {
			LOGGER.info("connecting");
			client.connect(folder, (event, filepath) -> {
				LOGGER.info("received " + event + " on " + filepath);
			});					
			
			Threads.sleep(60000);
			
			LOGGER.info("disconnecting");
			client.disconnect();
			
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, null, e);
			
		} finally {
		}
	}}
