package m09uf3.impl.fwatch;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import m09uf3.prob.fwatch.FWatchClient;
import m09uf3.prob.fwatch.FWatchEvent;
import m09uf3.prob.fwatch.FWatchHandler;

public class FWatchClientImpl implements FWatchClient, Runnable {
	private String host;
	private int port;
	private Socket socket;
	private OutputStream out;
	private InputStream in;
	Thread thread;
	String path;
	FWatchHandler handler;
	static final Logger LOGGER = Logger.getLogger(FWatchClientImpl.class.getName());
	static final Charset UTF8 = StandardCharsets.UTF_8;
	static final Charset ISO8859_1 = StandardCharsets.ISO_8859_1;
	static final Charset CHARSET = UTF8;

	public FWatchClientImpl(String host, int port) {
		this.host = host;
		this.port = port;
		this.thread = new Thread(this, "fwatch");
	}

	@Override
	public void connect(String path, FWatchHandler handler) throws IOException {
		this.socket = new Socket();
		this.socket.connect(new InetSocketAddress(host, port), 3000);
		this.out = socket.getOutputStream();
		this.in = socket.getInputStream();
		this.path = path;
		this.handler = handler;
		this.thread.start();
	}

	@Override
	public void disconnect() throws IOException {
		this.thread.interrupt();
		this.socket.close();
	}

	private void watcher(String path, FWatchHandler handler) throws IOException {
		String fileName = "C:/fwatch";
		byte[] fileNameArray = fileName.getBytes(StandardCharsets.UTF_8);
		byte[] arrayEnviament = new byte[fileNameArray.length + 1];
		arrayEnviament[0] = (byte) fileNameArray.length;
		for (int i = 0; i < fileNameArray.length; i++) {
			arrayEnviament[i + 1] = fileNameArray[i];
		}
		this.out.write(arrayEnviament);
		String t = "";
		int c = 0;
		byte[] archivo;
		do {
			byte[] b = new byte[1];
			b[0] = (byte) this.in.read();
			c = this.in.read();
			String tipo = new String(b, StandardCharsets.UTF_8);
			FWatchEvent fe = null;
			switch(tipo) {
				case "D":
					fe = FWatchEvent.DELETE;
					break;
				case "M":
					fe = FWatchEvent.MODIFY;
					break;
				case "C":
					fe = FWatchEvent.CREATE;
					break;
				default:
					System.out.println("Aqu� no deber�a de entrar");
					break;
			}
			archivo = new byte[c];
			for (int i = 0; i < c; i++) {
				archivo[i] = (byte) this.in.read();
			}
			handler.notify(fe, new String(archivo, StandardCharsets.UTF_8));
		} while(!this.socket.isClosed());
	}

	@Override
	public void run() {
		try {
			this.watcher(this.path, this.handler);
		} catch (IOException e) {
			if (socket.isClosed()) {
				LOGGER.info("Cliente desconectado");
			} else {
				e.printStackTrace();
			}
		}
	}
}
