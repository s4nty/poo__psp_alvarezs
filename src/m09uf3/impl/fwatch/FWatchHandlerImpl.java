package m09uf3.impl.fwatch;

import java.util.logging.Logger;

import m09uf3.prob.fwatch.FWatchEvent;
import m09uf3.prob.fwatch.FWatchHandler;

public class FWatchHandlerImpl implements FWatchHandler {
	static final Logger LOGGER = Logger.getLogger(FWatchHandlerImpl.class.getName());
	@Override
	public void notify(FWatchEvent event, String filePath) {
		LOGGER.info("Ha habido un evento de " + event + " en: " + filePath);
	}

}
