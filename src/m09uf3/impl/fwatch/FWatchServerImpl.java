package m09uf3.impl.fwatch;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import lib.Startable;
import m09uf3.prob.fwatch.FWatchEvent;
import m09uf3.prob.fwatch.FWatchHandler;
import m09uf3.prob.fwatch.FolderWatcher;

public class FWatchServerImpl implements Startable, Runnable {
	private String host;
	private int port;
	Thread thread;
	ServerSocket serverSocket;
	private boolean run;
	static final Logger LOGGER = Logger.getLogger(FWatchServerImpl.class.getName());
	static final Charset UTF8 = StandardCharsets.UTF_8;
	static final Charset ISO8859_1 = StandardCharsets.ISO_8859_1;
	static final Charset CHARSET = ISO8859_1;
	static final int TIMEOUT_MILLIS = 1500;
	public FWatchServerImpl(String host, int port) {
		this.host = host;
		this.port = port;
		this.run = true;
		this.thread = new Thread(this, "FWatchServerImpl");
	}
	@Override
	public void start() {
		try {
			this.serverSocket = new ServerSocket(this.port, 10, InetAddress.getByName(this.host));
			thread.start();
		} catch (IOException e) {
		}
	}

	@Override
	public void stop() {
		try {
			this.run = false;
			this.serverSocket.close();
		} catch (IOException e) {
			LOGGER.info("Error al cerrar el serverSocket");
		}
	}

	@Override
	public boolean isStarted() {
		return this.thread.isAlive();
	}

	@Override
	public void run() {
		while (this.run) {
			try {
				Socket clientSocket = serverSocket.accept();
				SocketAddress remote = clientSocket.getRemoteSocketAddress();					
				LOGGER.info("accept from " + remote);

				new Thread(() -> handle(clientSocket), "handle").start();
				
			} catch (IOException e) {
				if (serverSocket.isClosed()) {
					LOGGER.info("Servidor cerrado");
				} else {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void handle(Socket clientSocket) {
		try {
			OutputStream os = clientSocket.getOutputStream();
			InputStream is = clientSocket.getInputStream();
			int c = is.read();
			byte[] path = new byte[c];
			for(int i = 0; i < c; i++) {
				path[i] = (byte) is.read();
			}
			LOGGER.info(new String(path));
			FolderWatcher fw = new FolderWatcher(new String(path), new FWatchHandler() {
				@Override
				public void notify(FWatchEvent event, String filePath) {
					byte[] b = new byte[filePath.length() + 2];
					b[0] = getEvent(event);
					b[1] = (byte) filePath.length();
					for(int i = 0; i < filePath.length(); i++) {
						b[i + 2] = (byte) filePath.charAt(i);
					}
					try {
						System.out.println(new String(b));
						os.write(b);
					} catch (IOException e) {
						if (serverSocket.isClosed()) {
							LOGGER.info("Servidor cerrado");
						} else {
							e.printStackTrace();
						}
					}
				}
			});
			fw.start();
		} catch (IOException e) {
			System.out.println(e);
			LOGGER.info("Error en el handle");
		}
		
	}
	private byte getEvent(FWatchEvent event) {
		if(event == FWatchEvent.CREATE) return 0x43;
		if(event == FWatchEvent.MODIFY) return 0x4d;
		else return 0x44;
	}
}
