package m09uf3.impl.upper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import m09uf3.prob.upper.ToUpperClient;

public class ToUpperClientImpl implements ToUpperClient {
	private String host;
	private int port;
	private Socket socket;
	private PrintWriter out;
	private BufferedReader in;
	static final Logger LOGGER = Logger.getLogger(ToUpperClientImpl.class.getName());
	static final Charset UTF8 = StandardCharsets.UTF_8;
	static final Charset ISO8859_1 = StandardCharsets.ISO_8859_1;
	static final Charset CHARSET = UTF8;

	public ToUpperClientImpl(String host, int port) {
		this.host = host;
		this.port = port;
	}

	@Override
	public String toUpper(String input) throws IOException {
		out.println(input);
		String received = in.readLine();
		LOGGER.info("response is " + received);
		return received;
	}

	@Override
	public void connect() throws IOException {
		this.socket = new Socket();
		this.socket.connect(new InetSocketAddress(host, port), 3000);
		//this.socket.setSoTimeout(1000);
		this.out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), CHARSET), true);
		this.in = new BufferedReader(new InputStreamReader(socket.getInputStream(), CHARSET));
	}

	@Override
	public void disconnect() throws IOException {
		this.socket.close();
	}

}
