package m09uf3.impl.upper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

import m09uf3.prob.upper.ToUpperServer;

public class ToUpperServerImpl implements ToUpperServer, Runnable {
	private String host;
	private int port;
	private ServerSocket serverSocket;
	private Thread thread;
	private boolean runServer = true;
	static final Logger LOGGER = Logger.getLogger(ToUpperServerImpl.class.getName());
	static final int TIMEOUT_MILLIS = 1500;
	static final Charset UTF8 = StandardCharsets.UTF_8;
	static final Charset ISO8859_1 = StandardCharsets.ISO_8859_1;
	static final Charset CHARSET = UTF8;

	public ToUpperServerImpl(String host, int port) {
		this.host = host;
		this.port = port;
		this.thread = new Thread(this, "Hola");
	}

	@Override
	public void start() {
		try {
			this.serverSocket = new ServerSocket(port, 10, InetAddress.getByName(host));
			//serverSocket.setSoTimeout(TIMEOUT_MILLIS);
			thread.start();
		} catch (IOException e) {
			LOGGER.info("Error");
		}

	}

	@Override
	public void stop() {
		try {
			this.serverSocket.close();
			this.runServer = false;
		} catch (IOException e) {
			LOGGER.info("Error al para el servidor");
		}

	}

	static void handle(Socket clientSocket) throws IOException {
		try (PrintWriter out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream(), CHARSET), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), CHARSET))) {
			
			String input;
			while((input = in.readLine()) != null) {				
				LOGGER.info("request is " + input);
				out.println(input.toUpperCase());
			}

		} catch (SocketTimeoutException e) {
			LOGGER.fine("timeout reading");

		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "handling", e);
		}
	}

	@Override
	public void run() {
		while (this.runServer) {
			try {
				Socket clientSocket = serverSocket.accept();
				SocketAddress remote = clientSocket.getRemoteSocketAddress();
				LOGGER.info("accept from " + remote);
				//	clientSocket.setSoTimeout(TIMEOUT_MILLIS);
				handle(clientSocket);
			} catch (IOException e) {
				this.stop();
			}
		}

	}

}
