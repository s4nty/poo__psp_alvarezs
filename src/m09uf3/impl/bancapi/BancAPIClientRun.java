package m09uf3.impl.bancapi;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import lib.LoggingConfigurator;
import lib.Problems;
import lib.Reflections;
import m03uf6.prob.banc.BancDAO;
import m03uf6.prob.banc.Moviment;

public class BancAPIClientRun {
	
	static final Logger LOGGER = Logger.getLogger(BancAPIClientRun.class.getName());
	
	static final String HOST = "localhost";
	static final int PORT = 5513;
	
	static final String BASEURL = "http://" + HOST + ":" + PORT;
	
	static final BigDecimal BD100 = BigDecimal.valueOf(100);
	static final BigDecimal BD75 = BigDecimal.valueOf(75);
	static final BigDecimal BD50 = BigDecimal.valueOf(50);
	static final BigDecimal BD25 = BigDecimal.valueOf(25);
	
	public static void main(String[] args) {
		
		LoggingConfigurator.configure(Level.INFO);

		String packageName = Problems.getImplPackage(BancAPIClientRun.class);
		
		BancDAO dao = Reflections.newInstanceOfType(
				BancDAO.class, packageName + ".RemoteBancDAOImpl",
				new Class[] {String.class},
				new Object[] {BASEURL});
		
		try {
			dao.nouCompte(1, BD100);
			dao.nouCompte(2, BD50);
			dao.nouCompte(3, BD25);
			dao.transferir(1, 2, BD50);
			dao.transferir(2, 3, BD25);
			
			List<Moviment> l1 = dao.getMoviments(1);
			LOGGER.info("moviments 1: " + l1.toString());
			
			List<Moviment> l2 = dao.getMoviments(2);
			LOGGER.info("moviments 2: " + l2.toString());
			
			List<Moviment> l3 = dao.getMoviments(3);
			LOGGER.info("moviments 3: " + l3.toString());
			
			LOGGER.info("saldo 1: " + dao.getSaldo(1));
			LOGGER.info("saldo 2: " + dao.getSaldo(2));
			LOGGER.info("saldo 3: " + dao.getSaldo(3));
			
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "testing", e);
		}
	}
}
