package m09uf3.impl.bancapi;

import java.io.IOException;
import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import lib.db.DAOException;
import lib.db.DuplicateException;
import lib.db.NotFoundException;
import m03uf6.prob.banc.BancDAO;
import m03uf6.prob.banc.Moviment;
import m03uf6.prob.banc.SenseFonsException;

public class RemoteBancDAOImpl implements BancDAO {
	private String url;
	private JSONObject json;
	static final Logger LOGGER = Logger.getLogger(RemoteBancDAOImpl.class.getName());
	
	public RemoteBancDAOImpl(String url) {
		this.url = url;
	}

	public void init() {
		new RuntimeException("Metodo no valido");
	}

	public void nouCompte(int numero, BigDecimal saldoInicial) throws DuplicateException {
		try {
			json = new JSONObject().put("numero", numero);
			json.put("saldoInicial", saldoInicial);
			Response res = Jsoup.connect(url + "/compte").method(Method.POST).requestBody(json.toString())
					.ignoreContentType(true).ignoreHttpErrors(true).execute();
			if (res.statusCode() == 201) {
				LOGGER.info("Cuenta creada correctamente!");
			} else if (res.statusCode() == 409) {
				throw new DuplicateException("La cuenta " + numero + " ya existe");
			} else {
				throw new IOException("Error al crear la cuenta");
			}
		} catch (IOException e) {
			LOGGER.info("Error al crear la cuenta");
		}
	}

	@Override
	public int transferir(int origen, int desti, BigDecimal quantitat) throws NotFoundException, SenseFonsException {
		try {
			json = new JSONObject().put("origen", origen);
			json.put("desti", desti);
			json.put("quantitat", quantitat);
			Response res = Jsoup.connect(url + "/transferir").method(Method.PUT).requestBody(json.toString())
					.ignoreContentType(true).ignoreHttpErrors(true).execute();
			if (res.statusCode() == 200) {
				LOGGER.info("Transferencia hecha sin problemas");
				return new JSONObject(res.body()).getInt("id");

			} else if (res.statusCode() == 404) {
				throw new NotFoundException("Error, la cuenta no existe");

			} else if (res.statusCode() == 409) {
				throw new SenseFonsException("No hay fondos suficientes en la cuenta :(");

			} else {
				throw new DAOException("Bad request");
			}
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "requesting", e);
		}
		return -1;
	}

	public List<Moviment> getMoviments(int origen) {
		List<Moviment> moviments = null;
		try {
			Response res = Jsoup.connect(url + "/moviments/" + origen).method(Method.GET).ignoreContentType(true)
					.ignoreHttpErrors(true).execute();
			if (res.statusCode() == 200) {
				JSONArray jsoArray = new JSONArray(res.body());
				moviments = new ArrayList<Moviment>();
				for (int i = 0; i < jsoArray.length(); i++) {
					JSONObject object = jsoArray.getJSONObject(i);
					Moviment m = new Moviment();
					m.origen = object.getInt("origen");
					m.desti = object.getInt("desti");
					m.quantitat = object.getBigDecimal("quantitat");
					moviments.add(m);
				}
			} else {
				throw new IOException("Error");
			}
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "requesting", e);
		}
		return moviments;
	}

	@Override
	public BigDecimal getSaldo(int compte) throws NotFoundException {
		try {
			Response res = Jsoup.connect(url + "/saldo/" + compte).method(Method.GET).ignoreContentType(true)
					.ignoreHttpErrors(true).execute();
			if (res.statusCode() == 200) {
				LOGGER.info("Saldo obtenido correctamente");
				return new JSONObject(res.body()).getBigDecimal("saldo");
			} else {
				throw new IOException("Ups..." + res.statusCode());
			}
		} catch (Exception e) {
			throw new NotFoundException("La cuenta no existe");
		}
	}
}
