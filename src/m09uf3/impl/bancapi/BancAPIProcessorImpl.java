package m09uf3.impl.bancapi;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import jdk.nashorn.internal.runtime.JSONListAdapter;
import lib.db.DuplicateException;
import lib.db.NotFoundException;
import lib.http.HTTPProcessor;
import lib.http.HTTPRequest;
import lib.http.HTTPResponse;
import lib.http.HTTPUtils;
import lib.http.SocketHTTPServer;
import m03uf6.prob.banc.BancDAO;
import m03uf6.prob.banc.Moviment;
import m03uf6.prob.banc.SenseFonsException;

public class BancAPIProcessorImpl implements HTTPProcessor {
	static final Logger LOGGER = Logger.getLogger(BancAPIProcessorImpl.class.getName());
	static final String HOST = "localhost";
	static final int PORT = 8888;
	static final String BASEURL = "http://" + HOST + ":" + PORT;
	private BancDAO banc;
	
	public BancAPIProcessorImpl(BancDAO banc) {
		this.banc = banc;
	}

	public void process(HTTPRequest req, HTTPResponse res) {
		if ("/compte".equals(req.getPath()) && "POST".equals(req.getMethod())) {
			nuevaCuenta(req, res);
		} else if ("/saldo/".equals(req.getPath().substring(0, 7)) && "GET".equals(req.getMethod())) {
			getSaldo(req, res);
		} else if ("/transferir".equals(req.getPath()) && "PUT".equals(req.getMethod())) {
			nuevaTransferencia(req, res);
		} else if ("/moviments/".equals(req.getPath().substring(0, 11)) && "GET".equals(req.getMethod())) {
			getMoviments(req, res);
		} else {
			process(res, "400 Bad Request", null);
		}
	}

	private void process(HTTPResponse res, String status, String jsonStr) {
		PrintWriter out = res.getWriter();
		out.println("HTTP/1.1 " + status);
		out.println("Server: SantyAO");
		out.println("Date: " + HTTPUtils.toHTTPDate(new Date()));
		if (jsonStr != null) {
			out.println("Content-type: application/json; charset=" + SocketHTTPServer.CHARSET);
			out.println("Content-length: " + jsonStr.getBytes(SocketHTTPServer.CHARSET).length);
		}
		out.println(); // blank line!
		if (jsonStr != null) {
			out.println(jsonStr);
		}
	}

	private void nuevaCuenta(HTTPRequest req, HTTPResponse res) {
		String body = HTTPUtils.readToString(req.getReader());
		JSONObject j = new JSONObject(body);
		try {
			banc.nouCompte(j.getInt("numero"), j.getBigDecimal("saldoInicial"));
			process(res, "201 Created", null);
		} catch (DuplicateException e) {
			process(res, "409 Conflict", null);
		}
	}

	private void nuevaTransferencia(HTTPRequest req, HTTPResponse res) {
		String body = HTTPUtils.readToString(req.getReader());
		JSONObject j = new JSONObject(body);
		try {
			int id = banc.transferir(j.getInt("origen"), j.getInt("desti"), j.getBigDecimal("quantitat"));
			JSONObject jout = new JSONObject().put("id", id);
			process(res, "200 Ok", jout.toString());
		} catch (SenseFonsException e) {
			process(res, "409 Conflict", null);
		} catch (NotFoundException e) {
			process(res, "404 Not Found", null);
		}
	}

	private void getMoviments(HTTPRequest req, HTTPResponse res) {

		try {
			List<Moviment> llistaMoviments = banc.getMoviments(Integer.parseInt(req.getPath().substring(11)));
			JSONArray jsonArray = new JSONArray();
			for (Moviment moviment : llistaMoviments) {
				JSONObject jObject = new JSONObject();
				jObject.put("quantitat", moviment.quantitat);
				jObject.put("desti", moviment.desti);
				jObject.put("origen", moviment.origen);
				jsonArray.put(jObject);
			}
			process(res, "200 Ok", jsonArray.toString());
		} catch (NumberFormatException e) {
			process(res, "400 Bad Request", null);
		}
	}

	private void getSaldo(HTTPRequest req, HTTPResponse res) {
		try {
			BigDecimal saldo = banc.getSaldo(Integer.parseInt(req.getPath().substring(7)));
			JSONObject jout = new JSONObject().put("saldo", saldo);
			process(res, "200 Ok", jout.toString());
		} catch (NumberFormatException e) {
			process(res, "400 Bad Request", null);
		} catch (NotFoundException e) {
			process(res, "404 Not Found", null);
		}
	}
}
