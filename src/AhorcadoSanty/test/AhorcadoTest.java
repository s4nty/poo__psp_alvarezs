package AhorcadoSanty.test;

import static org.junit.Assert.*;

import org.junit.Test;
import AhorcadoSanty.ahorcado.Ahorcado;

public class AhorcadoTest {

	@Test
	public void test() {
		// Esto ya crea el tablero
		Ahorcado a = new Ahorcado();
		char[] palabraOculta = a.getPalabraOculta();
		char[] palabra = a.getPalabra();
		assertEquals(palabraOculta.length, palabra.length);
		int valorMaximo = a.getPalabras().length;
		int indexPalabra = a.getPalabraIndex();
		assertEquals(true, indexPalabra <= valorMaximo);
		int jugador = 1;
		assertEquals(a.turnoJugador(), jugador);
		assertEquals(palabraOculta.length, a.getPalabraOculta().length);
		assertEquals(palabra.length, a.getPalabra().length);
	}

}
