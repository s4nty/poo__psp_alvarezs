package AhorcadoSanty.ahorcado;

import javafx.application.*;
import javafx.stage.Stage;


public class app extends Application {
	private AhorcadoView av;
	private AhorcadoPresenter ap;
		
	public static void main(String[] args) {
		//new Ahorcado().run();
		launch(args);
	}
	
	public void start(Stage arg0) throws Exception {
		av = new AhorcadoView(arg0);
		ap = new AhorcadoPresenter();
		
		ap.setView(av);
		av.setPresenter(ap);
		
	}
}
