package AhorcadoSanty.ahorcado;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Ahorcado {
	private String[] palabras = {"piano", "ojear", "vanidoso", "temperatura", "torta", "sociedad", "copa", "garra", "positivo", "pipa"};
	Random rnd = new Random();
	Scanner reader = new Scanner(System.in);
	char[] palabraAdivinar;
	char[] palabraUsuario;
	private ArrayList<Character> palabrasUsadas;
	int jugador = 1;
	boolean sigueJugando = true;
	public Ahorcado() {
		crearTablero();
	}
	public void crearTablero() {
		palabrasUsadas = new ArrayList<Character>();
		String palabra = palabras[getPalabraIndex()];
		String palabraOculta = "";
		for(char c : palabra.toCharArray()) {
			// En mi caso ninguna palabra contiene un espacio en blanco, pero lo he hecho de tal manera de que funcione en el caso de que hubiera una con espacios.
			if(c == ' ') {
				palabraOculta += " ";
			} else {
				palabraOculta += "*";	
			}
			
		}
		//System.out.println(palabra);
		//System.out.println(palabraOculta);
		palabraAdivinar = palabra.toCharArray();
		palabraUsuario = palabraOculta.toCharArray();
		//int letrasNo = 1;
		//do {
			//letrasNo = nuevoTablero("s");
	//	} while(letrasNo != 0);
		//mostrarGanador();
	}	
	
	public int getPalabraIndex() {
		return rnd.nextInt(palabras.length); 
	}
	
	private char[] pedirLetra() {
		System.out.print("Introduce la letra a buscar: ");
		String c = reader.nextLine();
		return c.toCharArray();
	}
	
	public int nuevoTablero(String l) {
		//char letra = pedirLetra()[0];
		// La linea de abajo me gusta m�s, pero no creo que sea bueno tener que estar mostrando cada vez el jugador por consola
		//System.out.println("Turno del jugador " + (jugador = turnoJugador()));
		jugador = turnoJugador();
		char letra = l.toCharArray()[0];
		sigueJugando = false;
		char c = ' ';
		int contadorOc = 0;
		for(int i = 0; i < palabraAdivinar.length; i++) {
			c = palabraAdivinar[i];
			if(c == letra && !palabrasUsadas.contains(letra)) {
				sigueJugando = true;
				palabraUsuario[i] = c;
			} else if(palabraUsuario[i] == '*'){
				contadorOc++;
			}
		}
		if(!palabrasUsadas.contains(letra)) palabrasUsadas.add(letra);
		//System.out.println("" + new String(palabraUsuario));
		return contadorOc;
	}
	
	public int turnoJugador() {
		if(sigueJugando) {
			return (jugador == 1) ? 1 : 2;
		} else {
			return (jugador == 1) ? 2 : 1;
		}
	}
	
	private void mostrarGanador() {
		System.out.println("El ganador es el jugador: " + jugador + "!\nEnhorabuena!!");
	}
	
	public char[] getPalabraOculta() {
		return palabraUsuario;
	}
	
	public char[] getPalabra() {
		return palabraAdivinar;
	}
	
	public String[] getPalabras() {
		return palabras;
	}
	
	public String getLetrasUsadas() {
		return palabrasUsadas.toString();
	}
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	public void run() {
		crearTablero();
	}
	
	
}
