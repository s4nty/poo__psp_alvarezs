package AhorcadoSanty.ahorcado;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class AhorcadoView {
	private Label palabraOculta;
	private Label turnoActual;
	private Label letrasUsadas;
	private Label ganador;
	private Button btn;
	private Button empezar;
	private Button reset;
	private TextField txt;
	private AhorcadoPresenter ap;
	
	public AhorcadoView(Stage p) {
		//ap.generarPartida();
		p.setTitle("Adivinar la palabra | Santy �lvarez Osuca");
		empezar = new Button();
		txt = new TextField();
		txt.setVisible(false);
		btn = new Button();
		btn.setVisible(false);
		palabraOculta = new Label();
		palabraOculta.setVisible(false);
		turnoActual = new Label();
		turnoActual.setVisible(false);
		letrasUsadas = new Label();
		letrasUsadas.setVisible(false);
		reset = new Button();
		reset.setText("JUGAR DE NUEVO");
		reset.setVisible(false);
		ganador = new Label();
		ganador.setVisible(false);
		empezar.setText("EMPEZAR PARTIDA");
		empezar.setOnAction(new EventHandler<ActionEvent>() {
		    @Override 
		    public void handle(ActionEvent e) {
		    	empezarPartida();
		    }
		});
		btn.setOnAction(new EventHandler<ActionEvent>() {
		    @Override 
		    public void handle(ActionEvent e) {
		    	ap.comprueba(txt.getText());
		    	comprobar();
		    	txt.setText("");
		    	txt.requestFocus();
		    }
		});
		reset.setOnAction(new EventHandler<ActionEvent>() {
		    @Override 
		    public void handle(ActionEvent e) {
		    	resetPartida();
		    }
		});
		VBox root = new VBox();
		root.setAlignment(Pos.CENTER);
		root.getChildren().addAll(empezar, txt, btn, palabraOculta, turnoActual, letrasUsadas, ganador, reset);
		p.setScene(new Scene(root, 500, 500));
		p.show();
		
		
	}
	public void setPresenter(AhorcadoPresenter p) {
		ap = p;
		
	}
	
	public void setPalabraOculta(String palabra) {
		palabraOculta.setText(palabra);
	}
	
	public void setTurno(String turno) {
		turnoActual.setText("Turno del jugador: " + turno);
	}
	
	public void setLetrasUsadas(String letras) {
		letrasUsadas.setText(letras);
	}
	
	public void setGanador(String g) {
		ganador.setText("El ganador es: " + g);
	}
	
	public String getPalabra() {
		return ap.palabraOculta();
	}
	
	/* FUNCIONES PRIVADAS */
	
	private void empezarPartida() {
		txt.setText("");
    	btn.setText("COMPRUEBA");
    	palabraOculta.setText(ap.palabraOculta());
    	turnoActual.setText("Turno del jugador 1");
		cambiarVisibilidad(true);
    	empezar.setVisible(false);
	}
	
	private void resetPartida() {
		letrasUsadas.setText("");
		ap.generarPartida();
		empezarPartida();
	}
	
	private void cambiarVisibilidad(Boolean b) {
		txt.setVisible(b);
		btn.setVisible(b);
		palabraOculta.setVisible(b);
		turnoActual.setVisible(b);
		letrasUsadas.setVisible(b);
		reset.setVisible(!b);
		ganador.setVisible(!b);
	}
	
	private void comprobar() {
		if(ap.getTotalLetras() == 0) cambiarVisibilidad(false);
	}

}
