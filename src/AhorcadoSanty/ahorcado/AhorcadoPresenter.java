package AhorcadoSanty.ahorcado;

public class AhorcadoPresenter {
	private AhorcadoView av;
	private Ahorcado a;
	private int totalLetras;
	
	public AhorcadoPresenter() {
		a = new Ahorcado();
	}
	
	public void setView(AhorcadoView v) {
		av = v;
	}
	
	public void generarPartida() {
		a.crearTablero();
	}
	
	public void comprueba(String t) {
		//do {
		setTotalLetras(a.nuevoTablero(t));
		av.setPalabraOculta(new String(a.getPalabraOculta()));
		av.setTurno(String.valueOf(a.turnoJugador()));
		av.setLetrasUsadas(a.getLetrasUsadas());
		av.setGanador(String.valueOf(a.turnoJugador()));
		//} while(letrasNo != 0);
		
	}
	public String palabraOculta() {
		try {
			return new String(a.getPalabraOculta());
		} catch(Exception e) {
			return "Hola";
		}
		
	}
	
	public int getTotalLetras() {
		return totalLetras;
	}
	
	public void setTotalLetras(int n) {
		totalLetras = n;
	}
	
	public String test() {
		return "Test";
	}
}
