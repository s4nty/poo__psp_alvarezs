package examenes.examen2;

import javafx.application.Application;
import javafx.stage.Stage;
import examenes.examen2.CalcBoxContract.CalcBoxPresenter;
import examenes.examen2.CalcBoxContract.CalcBoxView;

public class app extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		CalcBoxView v = new CalcBoxViewImpl(primaryStage);
		CalcBoxPresenter p = new CalcBoxPresenterImpl();
		
		p.setView(v);
		v.setPresenter(p);
	}

	public static void main(String[] args) {
		launch(args);
	}
}
