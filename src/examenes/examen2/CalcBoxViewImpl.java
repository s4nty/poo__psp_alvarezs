package examenes.examen2;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import examenes.examen2.CalcBoxContract.CalcBoxPresenter;
import examenes.examen2.CalcBoxContract.CalcBoxView;

public class CalcBoxViewImpl implements CalcBoxView {

	private CalcBoxPresenter p;
	private Label label;
	private Button atras;
	private Button delante;
	private TextField text;

	public CalcBoxViewImpl(Stage stage) {
		initStage(stage);
	}
	
	private void initStage(Stage stage) {
		
		VBox vb = new VBox(10);
		
		Font font = new Font(20);
		
		text = new TextField();
		text.setFont(font);
		
		this.label = new Label();
		this.label.setFont(font);
		
		Button button = new Button("Calcula");
		button.setFont(font);
		button.setOnAction(event -> p.calcula(text.getText()));
		this.atras = new Button("<");
		this.atras.setFont(font);
		this.delante = new Button(">");
		this.delante.setFont(font);
		this.atras.setOnAction(event -> p.moverPuntero(-1, true));
		this.delante.setOnAction(event -> p.moverPuntero(1, true));
		disableButtons(true);
		vb.getChildren().addAll(text, this.label, button, this.atras, this.delante);
		Scene scene = new Scene(vb, 600, 400);
		stage.setScene(scene);
		stage.setTitle("Examen Santy");
		stage.show();
	}
	
	@Override
	public void setPresenter(CalcBoxPresenter p) {
		this.p = p;
	}

	@Override
	public void mostra(String display) {
		this.label.setText(display);
	}
	
	public void disableButtons(Boolean v) {
		this.atras.setDisable(v);
		this.delante.setDisable(v);
	}
	
	public void disableAtras(Boolean v) {
		this.atras.setDisable(v);
	}
	
	public void disableDelante(Boolean v) {
		this.delante.setDisable(v);
	}
	
	public void setText(String t) {
		this.text.setText(t);
	}
}
