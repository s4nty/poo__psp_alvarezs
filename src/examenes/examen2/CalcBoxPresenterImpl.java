package examenes.examen2;

import java.util.HashMap;

import m03uf5.impl.calcbox.ExprAvaluator2;
import examenes.examen2.CalcBoxContract.CalcBoxPresenter;
import examenes.examen2.CalcBoxContract.CalcBoxView;

public class CalcBoxPresenterImpl implements CalcBoxPresenter {

	private ExprAvaluator2 ea;
	private CalcBoxView v;
	private HashMap<Integer, String> e = new HashMap<Integer, String>();
	private int pos = 1;
	private int puntero = 1;
	public CalcBoxPresenterImpl() {			
		ea = new ExprAvaluator2();
	}
	
	@Override
	public void setView(CalcBoxView v) {
		this.v = v;
	}

	@Override
	public void calcula(String expr) {
		
		try {
			double valor = ea.avalua(expr);
			v.mostra(Double.toString(valor));
			e.put(pos, expr);
			puntero = pos;
			pos++;
			moverPuntero(0, false);
			
			
		} catch (Exception e) {
			v.mostra("Expresi�n incorrecta");
		}
		
	}
	
	public void moverPuntero(int move, Boolean control) {
		this.puntero += move;
		if(puntero == 1) {
			v.disableAtras(true);
			if(e.size() == 1) v.disableDelante(true);
			else v.disableDelante(false);
		} else if(puntero == e.size()) {
			v.disableAtras(false);
			v.disableDelante(true);
		} else {
			v.disableButtons(false);
		}
		// Esto lo hago para que cuando se ponga un calculo nuevo, no calcule dos veces lo mismo
		if(control) actualizarDatos(puntero);
	}
	
	private void actualizarDatos(int puntero) {
		// Vuelvo a calcular aqu�, ya que si voy a la funci�n calcular, lo volver�a a poner en el mapa
		String expresion = e.get(puntero);
		v.setText(expresion);
		try {
			v.mostra(Double.toString(ea.avalua(expresion)));
		} catch (Exception e) {
			v.mostra("Expresi�n incorrecta");
		}
	}
	
}
