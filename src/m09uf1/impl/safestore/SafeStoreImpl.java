package m09uf1.impl.safestore;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.json.JSONObject;

import lib.CryptoKeys;
import lib.TextFileStorage;
import m09uf1.prob.safestore.SafeStore;
import m09uf1.prob.safestore.WrongKeyException;

public class SafeStoreImpl implements SafeStore {
	static final String PADDING = "AES/CBC/PKCS5Padding";
	static final String HASH_ALGORITHM = "PBKDF2WithHmacSHA256";
	static final Charset CHARSET = StandardCharsets.UTF_8;
	
	private TextFileStorage tf;
	private String fileName;
	private Cipher cipherEnc;
	private JSONObject json;
	private boolean fileClosed;
	private byte[] dekInicial;
	private char[] passI;
	public SafeStoreImpl(String fileName) {
		this.fileName = fileName;
		this.json = new JSONObject();
		this.tf = new TextFileStorage();
		this.fileClosed = true;
	}

	@Override
	public File getPath() {
		return new File(this.tf.getReadAbsolutePath(this.fileName));
	}

	@Override
	public void init(char[] password) throws IOException {
		this.passI = password;
		try {
			SecureRandom sr = new SecureRandom();
			byte[] salt = new byte[16];
			byte[] kiv = new byte[16];
			byte[] div = new byte[16];
			sr.nextBytes(salt);
			sr.nextBytes(kiv);
			sr.nextBytes(div);
			IvParameterSpec params = new IvParameterSpec(kiv);
			byte[] kek = generateSecretHash(password, salt);
			SecretKey kekKey = new SecretKeySpec(kek, "AES");
			KeyGenerator keyG = KeyGenerator.getInstance("AES");
			keyG.init(256);
			dekInicial = keyG.generateKey().getEncoded();
			cipherEnc = Cipher.getInstance(PADDING);
			cipherEnc.init(Cipher.ENCRYPT_MODE, kekKey, params);
			byte[] encDek = cipherEnc.doFinal(kek);
			json.put("SALT", Base64.getEncoder().encodeToString(salt));
			json.put("DIV", Base64.getEncoder().encodeToString(div));
			json.put("KIV", Base64.getEncoder().encodeToString(kiv));
			json.put("DEK", Base64.getEncoder().encodeToString(encDek));
			this.tf.writeToFile(this.fileName, json.toString());
			this.fileClosed = false;
		} catch (GeneralSecurityException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void open(char[] password) throws IOException {
		if(this.fileClosed) {
			json = new JSONObject(this.tf.readFromFile(this.fileName));
			if(!checkPasswd(password)) {
				throw new IOException("Contrase�a incorrecta");
			}
			this.fileClosed = false;
		}
	}

	@Override
	public void close() {
		this.fileClosed = true;
	}

	@Override
	public boolean isClosed() {
		return this.fileClosed;
	}

	@Override
	public String get(String key) throws WrongKeyException {
		if(this.json.has(key)) {
			try {
				byte[] div = Base64.getDecoder().decode(json.getString("DIV"));
				byte[] salt = Base64.getDecoder().decode(json.getString("SALT"));
				byte[] dek = Base64.getDecoder().decode(json.getString("DEK"));
				byte[] kiv = Base64.getDecoder().decode(json.getString("KIV"));
				Cipher cipher = Cipher.getInstance(PADDING);
				SecretKeyFactory sf = SecretKeyFactory.getInstance(HASH_ALGORITHM);
				KeySpec ks = new PBEKeySpec(this.passI, salt, 16384, 256);
				SecretKey uncodedKEK = sf.generateSecret(ks);
				SecretKey kek = new SecretKeySpec(uncodedKEK.getEncoded(), "AES");
				IvParameterSpec params = new IvParameterSpec(kiv);
				cipher.init(Cipher.DECRYPT_MODE, kek, params);
				byte[] decryptDEK = cipher.doFinal(dek);
				SecretKey secDek = new SecretKeySpec(decryptDEK, "AES");
				byte[] valueByte = Base64.getDecoder().decode(json.getString(key));
				IvParameterSpec paramsDiv = new IvParameterSpec(div);
				cipher.init(Cipher.DECRYPT_MODE, secDek, paramsDiv);
				byte[] decVal = cipher.doFinal(valueByte);
				return new String(decVal, CHARSET);
				
			} catch(GeneralSecurityException e) {
				throw new WrongKeyException(e);
			}
		}
		return null;
	}

	@Override
	public void set(String key, String value) throws WrongKeyException {
		try {
			if(value == null) json.remove(key);
			else {
				byte[] div = Base64.getDecoder().decode(json.getString("DIV"));
				byte[] salt = Base64.getDecoder().decode(json.getString("SALT"));
				byte[] dek = Base64.getDecoder().decode(json.getString("DEK"));
				byte[] kiv = Base64.getDecoder().decode(json.getString("KIV"));
				Cipher cipher = Cipher.getInstance(PADDING);
				SecretKeyFactory sf = SecretKeyFactory.getInstance(HASH_ALGORITHM);
				KeySpec ks = new PBEKeySpec(this.passI, salt, 16384, 256);
				SecretKey uncKEK = sf.generateSecret(ks);
				SecretKey KEK = new SecretKeySpec(uncKEK.getEncoded(), "AES");
				IvParameterSpec paramSpecKIV = new IvParameterSpec(kiv);
				cipher.init(Cipher.DECRYPT_MODE, KEK, paramSpecKIV);			
				byte[] decDek = cipher.doFinal(dek);
				SecretKey secretDEK = new SecretKeySpec(decDek, "AES");			
				IvParameterSpec paramSpecDIV = new IvParameterSpec(div);
				cipher.init(Cipher.ENCRYPT_MODE, secretDEK, paramSpecDIV);			
				json.put(key, Base64.getEncoder().encodeToString(cipher.doFinal(value.getBytes(CHARSET))));
			}
			this.tf.writeToFile(this.fileName, json.toString());
			this.fileClosed = false;
		} catch(GeneralSecurityException e) {
			throw new WrongKeyException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void setPassword(char[] usepass) throws WrongKeyException {
		if(!Arrays.equals(this.passI, usepass)) {
			throw new WrongKeyException("Contrase�a incorrecta");
		}
		try {
			Cipher cipher = Cipher.getInstance(PADDING);
			SecretKeyFactory sf = SecretKeyFactory.getInstance(HASH_ALGORITHM);
			byte[] salt = Base64.getDecoder().decode(json.getString("SALT"));
			byte[] kiv = Base64.getDecoder().decode(json.getString("KIV"));
			KeySpec ks = new PBEKeySpec(this.passI, salt, 16384, 256);
			SecretKey uncKEK = sf.generateSecret(ks);
			SecretKey KEK = new SecretKeySpec(uncKEK.getEncoded(), "AES");
			IvParameterSpec paramSpec = new IvParameterSpec(kiv);
			cipher.init(Cipher.DECRYPT_MODE, KEK, paramSpec);			
			byte[] decDek = cipher.doFinal(this.dekInicial);
			KeySpec ks_new = new PBEKeySpec(usepass, salt, 16384, 256);
			SecretKey uncKek_new = sf.generateSecret(ks_new);
			SecretKey kek_new = new SecretKeySpec(uncKek_new.getEncoded(), "AES");
			cipher.init(Cipher.ENCRYPT_MODE, kek_new, paramSpec);			
			json.put("DEK", Base64.getEncoder().encodeToString(cipher.doFinal(decDek)));				
		} catch (GeneralSecurityException e) {
			throw new WrongKeyException(e);
		}	
	}

	// M�TODOS PRIVADOS
	private static byte[] generateSecretHash(char[] password, byte[] salt) throws GeneralSecurityException {
		SecretKeyFactory f = SecretKeyFactory.getInstance(HASH_ALGORITHM);
		KeySpec ks = new PBEKeySpec(password, salt, 16384, 256);
		SecretKey s = f.generateSecret(ks);
		return s.getEncoded();
	}
	
	private boolean checkPasswd(char[] password) {
		try {
			Cipher cipher = Cipher.getInstance(PADDING);
			SecretKeyFactory sf = SecretKeyFactory.getInstance(HASH_ALGORITHM);
			byte[] salt = Base64.getDecoder().decode(json.getString("SALT"));
			byte[] kiv = Base64.getDecoder().decode(json.getString("KIV"));
			KeySpec ks = new PBEKeySpec(password, salt, 16384, 256);
			SecretKey uncKEK = sf.generateSecret(ks);
			SecretKey kek = new SecretKeySpec(uncKEK.getEncoded(), "AES");
			IvParameterSpec params = new IvParameterSpec(kiv);
			cipher.init(Cipher.ENCRYPT_MODE, kek, params);
			byte[] encDEK = cipher.doFinal(dekInicial);
			byte[] jEncDEK = Base64.getDecoder().decode(json.getString("DEK"));
			return Arrays.equals(encDEK, jEncDEK);
		} catch (GeneralSecurityException e) {
			throw new RuntimeException(e);
		} 
	}

}
