package m09uf1.impl.speerb;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.function.Consumer;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import m09uf1.prob.speerb.SafePeer;

public class SafePeerImpl implements SafePeer {
	static final Logger LOGGER = Logger.getLogger(SafePeer.class.getName());
	static final String HASH_ALGORITHM = "RSA/ECB/PKCS1Padding";
	static final Charset CHARSET = StandardCharsets.UTF_8;
	private Consumer<String> consumer;
	private SecretKey sk;
	private PrivateKey ppk;
	private PublicKey puk;
	private Cipher encCipher, decCipher;
	private SafePeer safePeer;
	
	public SafePeerImpl(Consumer<String> consumer) {
		this.consumer = consumer;
	}

	@Override
	public PublicKey getPublicKey() {
		if(this.ppk == null || this.puk == null) generatePpkPuk();
		return this.puk;
	}

	@Override
	public void connectTo(SafePeer to) {
		try {
			this.safePeer = to;
			this.puk = this.safePeer.getPublicKey();
			KeyGenerator kg = KeyGenerator.getInstance("AES");
			kg.init(256);
			this.sk = kg.generateKey();
			Cipher cipherEncK = Cipher.getInstance(HASH_ALGORITHM);
			cipherEncK.init(Cipher.ENCRYPT_MODE, this.puk);
			initCiphers();
			this.safePeer.connectFrom(this, cipherEncK.doFinal(this.sk.getEncoded()));
		} catch (GeneralSecurityException e) {
			throw new RuntimeException("GeneralSecurityException | connectTo ", e);
		} 
	}

	@Override
	public void connectFrom(SafePeer from, byte[] cipheredSecret) {
		try {
			this.safePeer = from;
			Cipher cipherDecK = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipherDecK.init(Cipher.DECRYPT_MODE, this.ppk);
			this.sk = new SecretKeySpec(cipherDecK.doFinal(cipheredSecret), "AES");
			initCiphers();
		} catch (GeneralSecurityException e) {
			throw new RuntimeException("Error ", e);
		} 
	}

	@Override
	public void send(String message) {
		try { 			
			byte[] inputArr = message.getBytes(CHARSET);
			this.safePeer.receive(this.encCipher.doFinal(inputArr));
		} catch (GeneralSecurityException e) {
			throw new RuntimeException("ciphering", e);
		}
		
	}

	@Override
	public void receive(byte[] cipheredBytes) {
		try {			
			byte[] outputArr = this.decCipher.doFinal(cipheredBytes);
			this.consumer.accept(new String(outputArr, CHARSET));
		} catch (GeneralSecurityException e) {
			throw new RuntimeException("GeneralSecurityException | receive", e);
		}
		
	}
	
	// M�TODOS PRIVADOS
	
	private KeyPair keyPairGenerator(String algorithm, int size) throws NoSuchAlgorithmException {     
        KeyPairGenerator kpg = KeyPairGenerator.getInstance(algorithm);
        kpg.initialize(size);
        return kpg.generateKeyPair();      
    } 
	
	private void initCiphers() {
		initEncCipher();
		initDecCipher();
	}
	
	private void generatePpkPuk() {
		try {
			KeyPair kp = this.keyPairGenerator("RSA", 1024);
			this.puk = kp.getPublic();
			this.ppk = kp.getPrivate();
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("NoSuchAlgorithmException | generatePpkPuk", e);
		}	
	}
	
	private void initEncCipher() {
		try {
			this.encCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			this.encCipher.init(Cipher.ENCRYPT_MODE, this.sk);
		} catch (GeneralSecurityException e) {
			throw new RuntimeException("GeneralSecurityException | initEncCipher", e);
		}
		
	}
	
	private void initDecCipher() {
		try {
			this.decCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			this.decCipher.init(Cipher.DECRYPT_MODE, this.sk);
		} catch (GeneralSecurityException e) {
			throw new RuntimeException("GeneralSecurityException | initDecCipher", e);
		}
		
	}
}
