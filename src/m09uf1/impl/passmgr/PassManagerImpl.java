package m09uf1.impl.passmgr;

import lib.db.DuplicateException;
import lib.db.NotFoundException;
import m09uf1.prob.passmgr.PassManager;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.logging.Logger;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import org.json.JSONObject;

public class PassManagerImpl implements PassManager {
	static final Logger LOGGER = Logger.getLogger(PassManagerImpl.class.getName());
	static final byte[] SALT = new byte[] { 1, 2, 3 };
	static final String HASH_ALGORITHM = "PBKDF2WithHmacSHA256";
	private JSONObject jsonObject;

	public PassManagerImpl(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}

	@Override
	public void addEntry(String user, char[] pass) throws DuplicateException {
		if (this.jsonObject.has(user)) {
			throw new DuplicateException("Ese usuario ya existe!");
		}
		byte[] salt = new byte[16];
		SecretKey sc = this.getSecretKey(pass, salt);
		byte[] keybytes = sc.getEncoded();
		this.jsonObject.put(user, new JSONObject().put("salt", Base64.getEncoder().encodeToString(salt)).put("pass", Base64.getEncoder().encodeToString(keybytes)));

	}

	@Override
	public void deleteEntry(String user) throws NotFoundException {
		if (!this.jsonObject.has(user)) {
			throw new NotFoundException("No se ha encontrado al usuario.");
		}
		this.jsonObject.remove(user);
	}

	@Override
	public boolean checkPassword(String user, char[] pass) throws NotFoundException {
		if (!this.jsonObject.has(user)) {
			throw new NotFoundException("No se ha encontrado al usuario.");
		}
		try {
			SecretKeyFactory sf = SecretKeyFactory.getInstance(HASH_ALGORITHM);
			byte[] salt = Base64.getDecoder().decode(this.jsonObject.getJSONObject(user).getString("salt"));
	        KeySpec ks = new PBEKeySpec(pass, salt, 16384, 256);
	        SecretKey sc = sf.generateSecret(ks);
	        byte[] keybytes = sc.getEncoded();
	        byte[] keydeljsone =  Base64.getDecoder().decode(this.jsonObject.getJSONObject(user).getString("pass"));
			return Arrays.equals(keydeljsone, keybytes);
		} catch (NoSuchAlgorithmException e) {
			LOGGER.info("NoSuchAlgorithException");
		} catch (InvalidKeySpecException e) {
			LOGGER.info("InvalidKeySpecException");
		}
		return false;
        
        
	}

	@Override
	public boolean setPassword(String user, char[] oldpass, char[] newpass) throws NotFoundException {
		if (!this.jsonObject.has(user)) {
			throw new NotFoundException("No se ha encontrado al usuario.");
		}
		if (!checkPassword(user, oldpass)) {
			return false;
		} else {
			deleteEntry(user);
			try {
				addEntry(user, newpass);
			} catch (DuplicateException e) {
				throw new NotFoundException("Duplicado.");
			}
			return true;
		}
	}
	
	// METODOS PRIVADOS
	
	private SecretKey getSecretKey(char[] pass, byte[] salt) {
		try {
			SecretKeyFactory sf = SecretKeyFactory.getInstance(HASH_ALGORITHM);
			SecureRandom rnd = new SecureRandom();
			rnd.nextBytes(salt);
			KeySpec ks = new PBEKeySpec(pass, salt, 16384, 256);
			return sf.generateSecret(ks);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return null;
		
	}

}
