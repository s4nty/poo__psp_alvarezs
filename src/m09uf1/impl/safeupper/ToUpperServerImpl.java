package m09uf1.impl.safeupper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import lib.Startable;

public class ToUpperServerImpl implements Startable {

	static final Logger LOGGER = Logger.getLogger(ToUpperServerImpl.class.getName());
	static final int PORT = 5508;
	static final int NTHREADS = 10;
	static final Charset UTF8 = StandardCharsets.UTF_8;
	static final Charset ISO8859_1 = StandardCharsets.ISO_8859_1;
	static final Charset CHARSET = UTF8;
	static final int TIMEOUT_MILLIS = 1500;
	private String host;
	private int port;
	private ServerSocket serverSocket;
	private Thread thread;
	private MyCoDec mc;
	private volatile static boolean v;

	public ToUpperServerImpl(String host, int port, byte[] secretKey) throws IOException {
		this.host = host;
		this.port = port;
		this.mc = new MyCoDec(secretKey);
		//this.thread = new Thread("ToUpperServerImpl");
	}

	@Override
	public void start() {
		new Thread(() -> server(), "ToUpperServerImpl").start();
	}

	@Override
	public void stop() {
		try {
			v = false;
			this.serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public boolean isStarted() {
		return this.thread.isAlive();
	}

	private void server() {
		v = true;
		ExecutorService service = Executors.newCachedThreadPool();
		try {
			this.serverSocket = new ServerSocket(port, 10, InetAddress.getByName(host));
			while (true) {
				try {
					Socket clientSocket = serverSocket.accept();
					SocketAddress remote = clientSocket.getRemoteSocketAddress();
					service.submit(() -> handle(clientSocket, mc));

				} catch (SocketTimeoutException e) {
					// timeout in the accept: ignore
					LOGGER.fine("timeout accepting");
				}
			}
		} catch (IOException e) {
			if (serverSocket != null && serverSocket.isClosed()) {
				LOGGER.info("server closed");
			} else {
				LOGGER.log(Level.SEVERE, "accepting", e);
			}

		} finally {
			service.shutdown();
		}
	}

	static void handle(Socket clientSocket, MyCoDec codecImpl) {
		String input = "";
		try (PrintWriter out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream(), CHARSET), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), CHARSET))) {

			while (v) {
				try {
					input = in.readLine();
					if (input == null) {
						LOGGER.warning("Cliente desconectado!");
						return;
					}
					input = codecImpl.decode(input);
					out.println(codecImpl.encode(input.toUpperCase()));
				} catch (SocketTimeoutException e) {
					// read timeout: do nothing
					LOGGER.fine("timeout reading!");
				}
			}
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "handling", e);
		} finally {
			try {
				clientSocket.close();
			} catch (IOException e) {
				LOGGER.warning("closing: " + e.getMessage());
			}
		}
	}

}
