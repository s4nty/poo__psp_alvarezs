package m09uf1.impl.safeupper;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import lib.CoDec;
import lib.CoDecException;

public class MyCoDec implements CoDec<String, String> {
	//private byte[] keyBytes;
	private Cipher cipherEncode;
	private Cipher cipherDecode;
	static final Charset CHARSET = StandardCharsets.UTF_8;
	public MyCoDec(byte[] secretKey) throws CoDecException {
		try {
			SecretKey myKey = new SecretKeySpec(secretKey, "AES");
			cipherEncode = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipherEncode.init(Cipher.ENCRYPT_MODE, myKey);
			cipherDecode = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipherDecode.init(Cipher.DECRYPT_MODE, myKey);
		} catch (GeneralSecurityException e) {
			throw new CoDecException("Error " + e.getMessage());
			
		}	
	}
	@Override
	public String encode(String i) throws CoDecException {
		try {
			return Base64.getEncoder().encodeToString(cipherEncode.doFinal(i.getBytes()));
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new CoDecException("Error " + e.getMessage());
			
		}	
	}

	@Override
	public String decode(String o) throws CoDecException {
		try {
			return new String(cipherDecode.doFinal( Base64.getDecoder().decode(o)), CHARSET);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new CoDecException("Error " + e.getMessage());
		}
	}

}
