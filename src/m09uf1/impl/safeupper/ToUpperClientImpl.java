package m09uf1.impl.safeupper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import javax.crypto.SecretKey;

import lib.CoDecException;
import m09uf1.prob.safeupper.ToUpperClient;

public class ToUpperClientImpl implements ToUpperClient {
	private String host;
	private int port;
	private Socket socket;
	private PrintWriter out;
	private BufferedReader in;
	private MyCoDec sc;
	static final Logger LOGGER = Logger.getLogger(ToUpperClientImpl.class.getName());
	static final Charset UTF8 = StandardCharsets.UTF_8;
	static final Charset ISO8859_1 = StandardCharsets.ISO_8859_1;
	static final Charset CHARSET = UTF8;
	public ToUpperClientImpl(String host, int port, byte[] sc) throws CoDecException {
		this.host = host;
		this.port = port;
		this.sc = new MyCoDec(sc);
	}
	@Override
	public String toUpper(String input) throws IOException {
		out.println(sc.encode(input));
		String r = in.readLine();
		LOGGER.info("Respuesta codificada: " + r);
		r = sc.decode(r);
		LOGGER.info("Respuesta decodificada: " + r);		
		return r;
	}

	@Override
	public void connect() throws IOException {
		this.socket = new Socket();
		this.socket.connect(new InetSocketAddress(host, port), 3000);
		//this.socket.setSoTimeout(1000);
		this.out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), CHARSET), true);
		this.in = new BufferedReader(new InputStreamReader(socket.getInputStream(), CHARSET));
		
	}

	@Override
	public void disconnect() throws IOException {
		this.socket.close();
	}

}
